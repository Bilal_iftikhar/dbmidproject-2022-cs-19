﻿using Guna.UI2.WinForms.Suite;
using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class projects : UserControl
    {
        public string titleU;
        public string descU;
        public projects()
        {
           
            InitializeComponent();
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView2.ColumnHeadersHeight = 35;
            guna2DataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //VIEW
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.Title,p.Description From Project p ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //INSERT
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = true;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //DELETE
          
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //UPDATE
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = true;
            delpanel.Visible = true;
            UAPANEL.Visible = true;
            guna2DataGridView3.MultiSelect = false;
            guna2DataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.Title,p.Description From Project p ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView3.DataSource = dt;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //BACKFROMVIEW
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            //backfromaddP
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
        }

        private void guna2Button11_Click(object sender, EventArgs e)
        {
            //addPbtn
            try
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Project values (@description, @Title); ", con);
                cmd.Parameters.AddWithValue("@Title", (titlebox.Text));
                cmd.Parameters.AddWithValue("@description", descriptionbox.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
               
            }
            catch
            {
                MessageBox.Show("An Error Occured While Running the Query");
            }
        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            //BACKFROMDEL
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //DELBTN
        }
        public int getID()
        {
            try
            {
                int id = 0;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT Id FROM Project WHERE Title = @Title", con);
                cmd.Parameters.AddWithValue("@Title", titleU);


                id = (int)cmd.ExecuteScalar();
                return id;
            }
            catch
            {
                return -1;
            }

        }

        private void guna2Button10_Click(object sender, EventArgs e)
        {
            //BACKFROMRPDATE
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            //UPDATE
            var con = Configuration.getInstance().getConnection();
            int id = getID();
            if (id > 0)
            {
                SqlCommand cmd = new SqlCommand("UPDATE Project SET Description = @Description, Title = @Title WHERE Id = @Id;", con);
                cmd.Parameters.AddWithValue("@Title", guna2TextBox2.Text);
                cmd.Parameters.AddWithValue("@Description", guna2TextBox1.Text);
                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Updated");
          
            }
            else
            {
                MessageBox.Show("Please Select a column to update first");

            }
        }

        private void guna2DataGridView3_Click(object sender, EventArgs e)
        {

        }

        private void guna2DataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                guna2DataGridView3.Rows[e.RowIndex].Selected = true;
                DataGridViewRow selectedRow = guna2DataGridView3.Rows[e.RowIndex];
                titleU = selectedRow.Cells["Title"].Value.ToString();
                descU = selectedRow.Cells["Description"].Value.ToString();
                guna2TextBox2.Text = titleU;
                guna2TextBox1.Text = descU;
            }
        }
    }
}
