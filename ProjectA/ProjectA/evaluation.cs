﻿using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class evaluation : UserControl
    {
        public evaluation()
        {
            InitializeComponent();
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView2.ColumnHeadersHeight = 35;
            guna2DataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //view
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.Name,p.TotalMarks,p.TotalWeightage From Evaluation p", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //insert
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = true;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //delte
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = true;
            delpanel.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.Name,p.TotalMarks,p.TotalWeightage From Evaluation p", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //update
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.Name,p.TotalMarks,p.TotalWeightage From Evaluation p", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //backfromview
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            //BACKFROMINSERT
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
        }

        private void guna2Button11_Click(object sender, EventArgs e)
        {
            //ADDEVAL
        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            //BAKCFROMEVALUATION
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //DELTEEVALUATION
        }

        private void guna2Button10_Click(object sender, EventArgs e)
        {
            //BACKFROMUPDATE
            evacrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible=false;
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            //UPDATE
        }
    }
}
