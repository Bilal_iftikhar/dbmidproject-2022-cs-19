﻿using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class markevaluation : UserControl
    {
        public markevaluation()
        {
            InitializeComponent();
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView2.ColumnHeadersHeight = 35;
            guna2DataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //VIEW
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.GroupId,e.Name,p.ObtainedMarks,p.EvaluationDate From GroupEvaluation p Join Evaluation e on p.GroupId = e.Id ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //INSERT
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible =true;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //DELETE
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = false;
            delpanel .Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.GroupId,e.Name,p.ObtainedMarks,p.EvaluationDate From GroupEvaluation p Join Evaluation e on p.GroupId = e.Id ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //UPADTE
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = true;
            delpanel.Visible = true;
            UAPANEL.Visible=true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.GroupId,e.Name,p.ObtainedMarks,p.EvaluationDate From GroupEvaluation p Join Evaluation e on p.GroupId = e.Id ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //BACKFROMVIEW
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
        }

        private void delpanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            //BACKFROMADD
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible=false;
        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            //BACKFROMDELTE
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
        }
        private void guna2Button10_Click(object sender, EventArgs e)
        {
            //BACKFROMUPADATE
            EVAMcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }
        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //DELETEBTN
        }
        private void guna2Button8_Click(object sender, EventArgs e)
        {
            //UPDATEBTN
            
        }
        private void guna2Button11_Click(object sender, EventArgs e)
        {
            //MARKBTN
        }
    }
}
