﻿using Guna.UI2.WinForms;
using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class advisor : UserControl
    {
        public string selectedfNameU;
        public string selectedlNameU;
        public string selectedconU;
        public string selectedmailU;
        public string selecteddobU;
        public int selectedgenU;
        public string selectedregU;
        public DateTime Dob;
        public string date;
        public string gender;
        public string designation;
        public string salary;
        public string des;
        public advisor()
        {
            InitializeComponent();
        
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView2.ColumnHeadersHeight = 35;
            guna2DataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;    
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //viewA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible=true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FirstName, LastName, L1.Value AS Designation, A.Salary, L.Value AS Gender, DateOfBirth AS [DOB], Contact, Email FROM Person P JOIN Advisor A ON A.Id = P.Id JOIN Lookup L ON L.Id = P.Gender JOIN Lookup L1 ON L1.Id = A.Designation", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }
        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //BACKFROMVIEWA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //insertA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible=true; 
        }
        private void guna2Button6_Click(object sender, EventArgs e)
        {
            //BACKFROMaddA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
        }
        private void guna2Button11_Click(object sender, EventArgs e)
        {
            //addadvisorbtn
            getGender();
            Dob = DateTime.Parse(dobbox.Text);
            int designation = getDesignation(designationcombobox.Text);
            var con = Configuration.getInstance().getConnection();
            int salary = int.Parse(salaraybox.Text);
            SqlCommand cmd = new SqlCommand("INSERT INTO Person(FirstName, LastName, Contact, Email, DateOfBirth,Gender) VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender); INSERT INTO Advisor(Id, Designation, Salary) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName=@LastName AND Contact=@Contact AND Email=@Email AND DateOfBirth=@DateOfBirth AND Gender=@Gender) , @Designation, @Salary);", con);
            cmd.Parameters.AddWithValue("@FirstName", fnamebox.Text);
            cmd.Parameters.AddWithValue("@LastName", Lnamebox.Text);
            cmd.Parameters.AddWithValue("@Contact", contatcbox.Text);
            cmd.Parameters.AddWithValue("@Email", mailbo.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", Dob);
            cmd.Parameters.AddWithValue("@Gender", selectedgenU);
            cmd.Parameters.AddWithValue("@Designation", designation);
            cmd.Parameters.AddWithValue("@Salary", salary);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
           
        }
        public int getDesignation(string designation)
        {
            int d = -1;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Lookup WHERE Category ='DESIGNATION' AND Value=@designation", con);
            cmd.Parameters.AddWithValue("@designation", designation);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                d = int.Parse(reader["Id"].ToString());
            }
            reader.Close();
            return d;
        }
        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //deleteA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = true;
            delpanel.Visible = true;
            guna2DataGridView2.MultiSelect = false;
            guna2DataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FirstName, LastName, L1.Value AS Designation, A.Salary, L.Value AS Gender,DateOfBirth AS [DOB], Contact, Email FROM Person P JOIN Advisor A ON A.Id = P.Id JOIN Lookup L ON L.Id = P.Gender JOIN Lookup L1 ON L1.Id = A.Designation", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView2.DataSource = dt;
        }
        private void guna2Button9_Click(object sender, EventArgs e)
        {
            //BACKFROMDELTEA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
        }
        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //DELTEA
            if (guna2DataGridView2.SelectedRows.Count > 0)
            {
                selectedfNameU = guna2DataGridView2.SelectedRows[0].Cells["FirstName"].Value.ToString();
                selectedlNameU = guna2DataGridView2.SelectedRows[0].Cells["LastName"].Value.ToString();
                selectedconU = guna2DataGridView2.SelectedRows[0].Cells["Contact"].Value.ToString();
                selectedmailU = guna2DataGridView2.SelectedRows[0].Cells["Email"].Value.ToString();
                date = guna2DataGridView2.SelectedRows[0].Cells["DOB"].Value.ToString();
                gender = guna2DataGridView2.SelectedRows[0].Cells["Gender"].Value.ToString();
                designation = guna2DataGridView2.SelectedRows[0].Cells["Designation"].Value.ToString();
                salary = guna2DataGridView2.SelectedRows[0].Cells["Salary"].Value.ToString();
                int id = getID();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete From Advisor where Id = @Id", con);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Deleted");
            }
            else
            {
                MessageBox.Show("No Row Selected");
            }
        }
        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //updateA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = true; 
            delpanel.Visible = true;
            UAPANEL.Visible = true;
            guna2DataGridView3.MultiSelect = false;
            guna2DataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FirstName, LastName, L1.Value AS Designation, A.Salary, L.Value AS Gender, DateOfBirth AS [DOB], Contact, Email FROM Person P JOIN Advisor A ON A.Id = P.Id JOIN Lookup L ON L.Id = P.Gender JOIN Lookup L1 ON L1.Id = A.Designation", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView3.DataSource = dt;
        }
        private void guna2Button10_Click(object sender, EventArgs e)
        {
            //BACKFROMUA
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }
        private void guna2Button8_Click(object sender, EventArgs e)
        {
            
        }
        void getGender()
        {
           
            if (genderbox.Text == "Male")
            {
                selectedgenU = 1;
            }
            else
            {
                selectedgenU = 2;
            }

        }
        public int getID()
        {
            int id = 0;
            getGender();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Person WHERE  FirstName = @FirstName AND LastName = @LastName AND Contact = @Contact AND Email = @Email", con);
            cmd.Parameters.AddWithValue("@FirstName", selectedfNameU);
            cmd.Parameters.AddWithValue("@LastName", selectedlNameU);
            cmd.Parameters.AddWithValue("@Contact", selectedconU);
            cmd.Parameters.AddWithValue("@Email", selectedmailU);
            cmd.Parameters.AddWithValue("@Gender", selectedgenU);
            id = (int)cmd.ExecuteScalar();
            return id;
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void genderbox_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void guna2Button10_Click_2(object sender, EventArgs e)
        {
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;
        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button8_Click_1(object sender, EventArgs e)
        {
            //UABTN
            getGender();
            Dob = DateTime.Parse(guna2DateTimePicker1.Text);
            var con = Configuration.getInstance().getConnection();
            int id = getID();
            int sal = int.Parse(guna2TextBox1.Text);
            int designation = getDesignation(guna2ComboBox1.Text);
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DateOfBirth, Gender = @Gender WHERE Id = @Id; UPDATE Advisor SET Designation = @Designation ,Salary = @Salary WHERE Id = @Id", con);
            cmd.Parameters.AddWithValue("@FirstName", (guna2TextBox5.Text));
            cmd.Parameters.AddWithValue("@LastName", guna2TextBox4.Text);
            cmd.Parameters.AddWithValue("@Contact", guna2TextBox2.Text);
            cmd.Parameters.AddWithValue("@Email", guna2TextBox3.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", Dob);
            cmd.Parameters.AddWithValue("@Gender", selectedgenU);
            cmd.Parameters.AddWithValue("@Designation", designation);
            cmd.Parameters.AddWithValue("@Salary",sal );
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Record Update");
        }

        private void guna2DataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                guna2DataGridView3.ClearSelection();
                guna2DataGridView3.Rows[e.RowIndex].Selected = true;
                DataGridViewRow selectedRow = guna2DataGridView3.Rows[e.RowIndex];
                selectedfNameU = selectedRow.Cells["FirstName"].Value.ToString();
                selectedlNameU = selectedRow.Cells["LastName"].Value.ToString();
                des = selectedRow.Cells["Designation"].Value.ToString();
                salary = selectedRow.Cells["Salary"].Value.ToString();
                gender = selectedRow.Cells["Gender"].Value.ToString();
                date = selectedRow.Cells["DOB"].Value.ToString();
                selectedconU = selectedRow.Cells["Contact"].Value.ToString();
                selectedmailU = selectedRow.Cells["Email"].Value.ToString();
                guna2TextBox5.Text = selectedfNameU;
                guna2TextBox4.Text = selectedlNameU;
                guna2TextBox2.Text = selectedconU;
                guna2TextBox3.Text = selectedmailU;
                guna2TextBox1.Text = salary;
                guna2DateTimePicker1.Text = date;
                Dob = DateTime.Parse(date);
                guna2ComboBox1.Text = des;
               
               
                if (gender == "Male")
                {
                    guna2ComboBox2.Text = "Male";
                }
                else
                {
                    guna2ComboBox2.Text = "Female";
                }
            }
        }
    }
}
