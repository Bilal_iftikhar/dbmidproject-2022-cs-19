﻿using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using System.Xml.Linq;
using Guna.UI2.WinForms;

namespace ProjectA
{
    public partial class student : UserControl
    {
        public string selectedfNameU;
        public string selectedlNameU;
        public string selectedconU;
        public string selectedmailU;
        public string selecteddobU;
        public int selectedgenU;
        public string selectedregU;
        public DateTime Dob;
        public string date;
        public string gender;
        public student()
        {

            InitializeComponent();
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView2.ColumnHeadersHeight = 35;
            guna2DataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible = false;

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //VIEW
            VIEWAPANEL.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.RegistrationNo,p.FirstName,p.LastName,p.Contact,p.Email,p.DateOfBirth,p.Gender From Person p Join Student s on p.Id = s.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //INSERT
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = true;
     
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //DELETE
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = true;
            delpanel.Visible = true;
            guna2DataGridView2.MultiSelect = false;
            guna2DataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.RegistrationNo,p.FirstName,p.LastName,p.Contact,p.Email,p.DateOfBirth,p.Gender From Person p Join Student s on p.Id = s.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView2.DataSource = dt;
         
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //UPDATE
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = true;
            insertpanel.Visible = true;
            delpanel.Visible = true;
            UAPANEL.Visible = true;
            guna2DataGridView3.MultiSelect = true;
            guna2DataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.RegistrationNo,p.FirstName,p.LastName,p.Contact,p.Email,p.DateOfBirth,p.Gender From Person p Join Student s on p.Id = s.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView3.DataSource = dt;
            if (guna2DataGridView3.SelectedRows.Count > 0)
            {
              

                selectedregU = guna2DataGridView3.SelectedRows[0].Cells["RegistrationNo"].Value.ToString();
                selectedfNameU = guna2DataGridView3.SelectedRows[0].Cells["FirstName"].Value.ToString();
                selectedlNameU = guna2DataGridView3.SelectedRows[0].Cells["LastName"].Value.ToString();
                selectedconU = guna2DataGridView3.SelectedRows[0].Cells["Contact"].Value.ToString();
                selectedmailU = guna2DataGridView3.SelectedRows[0].Cells["Email"].Value.ToString();
                date = guna2DataGridView3.SelectedRows[0].Cells["DateOfBirth"].Value.ToString();
                gender = guna2DataGridView3.SelectedRows[0].Cells["Gender"].Value.ToString();
                guna2TextBox5.Text = selectedfNameU;
                guna2TextBox4.Text = selectedlNameU;
                guna2TextBox2.Text = selectedconU;
                guna2TextBox3.Text = selectedmailU;
                guna2TextBox1.Text = selectedregU;
                guna2DateTimePicker1.Text = date;
                Dob = DateTime.Parse(date);
                if (gender == "1")
                {
                    guna2ComboBox2.Text = "Male";
                }
                else
                {
                    guna2ComboBox2.Text = "Female";
                }
            }
            else
            {
                MessageBox.Show("No Row Selected");
            }
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //BACKFROMVIEW
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            //BACKFROMADD
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible     =false;
        }

        private void guna2Button11_Click(object sender, EventArgs e)
        {
            //ADDBTN
            getGender();
            Dob = DateTime.Parse(dobbox.Text);
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender)", con);
            cmd.Parameters.AddWithValue("@FirstName", (fnamebox.Text));
            cmd.Parameters.AddWithValue("@LastName", Lnamebox.Text);
            cmd.Parameters.AddWithValue("@Contact", contatcbox.Text);
            cmd.Parameters.AddWithValue("@Email", mailbo.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", Dob);
            cmd.Parameters.AddWithValue("@Gender", selectedgenU);
            cmd.ExecuteNonQuery();
            int Id = GetMaxPersonId();
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Insert into Student values (@Id, @RegisterationNo)", con2);
            cmd2.Parameters.AddWithValue("@Id", Id);
            cmd2.Parameters.AddWithValue("@RegisterationNo", REGNOBOX.Text);
            cmd2.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            //BACKFROMDLT
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //DELETEBTN
            if (guna2DataGridView2.SelectedRows.Count > 0)
            {
                selectedregU = guna2DataGridView2.SelectedRows[0].Cells["RegistrationNo"].Value.ToString();
                selectedfNameU = guna2DataGridView2.SelectedRows[0].Cells["FirstName"].Value.ToString();
                selectedlNameU = guna2DataGridView2.SelectedRows[0].Cells["LastName"].Value.ToString();
                selectedconU = guna2DataGridView2.SelectedRows[0].Cells["Contact"].Value.ToString();
                selectedmailU = guna2DataGridView2.SelectedRows[0].Cells["Email"].Value.ToString();
                date = guna2DataGridView2.SelectedRows[0].Cells["DateOfBirth"].Value.ToString();
                gender = guna2DataGridView2.SelectedRows[0].Cells["Gender"].Value.ToString();
                int id = getID();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete From Student where Id = @Id; Delete From Person where Id = @Id ", con);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Deleted");
            }
            else
            {
                MessageBox.Show("No Row Selected");
            }

        }

        private void guna2Button10_Click(object sender, EventArgs e)
        {
            //backfromupdate
            ADVISORcrudpanel.Visible = true;
            VIEWAPANEL.Visible = false;
            insertpanel.Visible = false;
            delpanel.Visible = false;
            UAPANEL.Visible=false;
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            //updatebtn
            getGender();
            Dob = DateTime.Parse(guna2DateTimePicker1.Text);
            var con = Configuration.getInstance().getConnection();
            int id = getID();
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DateOfBirth, Gender = @Gender WHERE Id = @Id", con);
            cmd.Parameters.AddWithValue("@FirstName", (guna2TextBox5.Text));
            cmd.Parameters.AddWithValue("@LastName", guna2TextBox4.Text);
            cmd.Parameters.AddWithValue("@Contact", guna2TextBox2.Text);
            cmd.Parameters.AddWithValue("@Email", guna2TextBox3.Text);
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.Parameters.AddWithValue("@DateOfBirth", Dob);
            cmd.Parameters.AddWithValue("@Gender", selectedgenU);
            cmd.ExecuteNonQuery();
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("UPDATE Student SET Id = @Id, RegisterationNo = @RegisterationNo", con2);
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.Parameters.AddWithValue("@RegisterationNo", guna2TextBox1.Text);
            MessageBox.Show("Successfully Updated");
        }

        void getGender()
        {
            if (genderbox.Text == "Male")
            {
                selectedgenU = 1;
            }
            else
            {
                selectedgenU = 2;
            }

        }
       public int GetMaxPersonId()
       {
            string selectMaxIdQuery = "SELECT Top 1 Id FROM Person ORDER BY Id DESC ;";
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand(selectMaxIdQuery, con);
            return (int)command.ExecuteScalar();
       }
       public int getID()
        {
            int id = 0;
            getGender();
            Dob = DateTime.Parse(dobbox.Text);
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Person WHERE  FirstName = @FirstName AND LastName = @LastName AND Contact = @Contact AND Email = @Email AND Gender = @Gender", con);
            cmd.Parameters.AddWithValue("@FirstName", selectedfNameU);
            cmd.Parameters.AddWithValue("@LastName", selectedlNameU);
            cmd.Parameters.AddWithValue("@Contact", selectedconU);
            cmd.Parameters.AddWithValue("@Email", selectedmailU);
            cmd.Parameters.AddWithValue("@DateOfBirth", Dob);
            cmd.Parameters.AddWithValue("@Gender", gender);
            id = (int)cmd.ExecuteScalar();
            return id;
        }

        private void guna2DataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                guna2DataGridView3.ClearSelection();
                guna2DataGridView3.Rows[e.RowIndex].Selected = true;

                DataGridViewRow selectedRow = guna2DataGridView3.Rows[e.RowIndex];

                // Accessing values using the correct DataGridView (guna2DataGridView3)
                selectedregU = selectedRow.Cells["RegistrationNo"].Value.ToString();
                selectedfNameU = selectedRow.Cells["FirstName"].Value.ToString();
                selectedlNameU = selectedRow.Cells["LastName"].Value.ToString();
                selectedconU = selectedRow.Cells["Contact"].Value.ToString();
                selectedmailU = selectedRow.Cells["Email"].Value.ToString();
                date = selectedRow.Cells["DateOfBirth"].Value.ToString();
                gender = selectedRow.Cells["Gender"].Value.ToString();
                guna2TextBox5.Text = selectedfNameU;
                guna2TextBox4.Text = selectedlNameU;
                guna2TextBox2.Text = selectedconU;
                guna2TextBox3.Text = selectedmailU;
                guna2TextBox1.Text = selectedregU;
                guna2DateTimePicker1.Text = date;
                Dob = DateTime.Parse(date);

                if (gender == "1")
                {
                    guna2ComboBox2.Text = "Male";
                }
                else
                {
                    guna2ComboBox2.Text = "Female";
                }
            }
        }
    }
}
