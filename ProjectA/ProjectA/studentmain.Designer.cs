﻿namespace ProjectA
{
    partial class studentmain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.viewpanel = new System.Windows.Forms.Panel();
            this.insertpanel = new System.Windows.Forms.Panel();
            this.deletepanel = new System.Windows.Forms.Panel();
            this.updatepanel = new System.Windows.Forms.Panel();
            this.updatepaneloptions = new System.Windows.Forms.Panel();
            this.unamepanel = new System.Windows.Forms.Panel();
            this.updatefnamebox = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel9 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.guna2Button12 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button25 = new Guna.UI2.WinForms.Guna2Button();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.guna2TextBox2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel10 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.guna2Button20 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button21 = new Guna.UI2.WinForms.Guna2Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel11 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel8 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.guna2Button19 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button18 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button17 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button16 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button15 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button14 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button13 = new Guna.UI2.WinForms.Guna2Button();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel7 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.guna2DataGridView3 = new Guna.UI2.WinForms.Guna2DataGridView();
            this.guna2Button9 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button10 = new Guna.UI2.WinForms.Guna2Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel6 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.guna2DataGridView2 = new Guna.UI2.WinForms.Guna2DataGridView();
            this.guna2Button8 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button7 = new Guna.UI2.WinForms.Guna2Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel5 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.genderbox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.dobbox = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.contatcbox = new Guna.UI2.WinForms.Guna2TextBox();
            this.mailbo = new Guna.UI2.WinForms.Guna2TextBox();
            this.Lnamebox = new Guna.UI2.WinForms.Guna2TextBox();
            this.fnamebox = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2Button11 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button6 = new Guna.UI2.WinForms.Guna2Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel3 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.guna2DataGridView1 = new Guna.UI2.WinForms.Guna2DataGridView();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel4 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2GradientPanel2 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel1 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ulnamepanel = new System.Windows.Forms.Panel();
            this.UCONTPANEL = new System.Windows.Forms.Panel();
            this.umailpanel = new System.Windows.Forms.Panel();
            this.UPDATEMPANEL = new System.Windows.Forms.Panel();
            this.genderUpanel = new System.Windows.Forms.Panel();
            this.guna2ComboBox2 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2GradientPanel28 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.guna2Button39 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button40 = new Guna.UI2.WinForms.Guna2Button();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel73 = new System.Windows.Forms.Panel();
            this.panel74 = new System.Windows.Forms.Panel();
            this.guna2TextBox8 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel29 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.guna2Button41 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button42 = new Guna.UI2.WinForms.Guna2Button();
            this.panel75 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panel78 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel30 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label48 = new System.Windows.Forms.Label();
            this.panel79 = new System.Windows.Forms.Panel();
            this.panel80 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel31 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label49 = new System.Windows.Forms.Label();
            this.guna2DateTimePicker2 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2GradientPanel24 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.guna2Button35 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button36 = new Guna.UI2.WinForms.Guna2Button();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.guna2TextBox9 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel25 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.guna2Button37 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button38 = new Guna.UI2.WinForms.Guna2Button();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel69 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel26 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label42 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel27 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label43 = new System.Windows.Forms.Label();
            this.guna2TextBox6 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel20 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.guna2Button31 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button32 = new Guna.UI2.WinForms.Guna2Button();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.guna2TextBox7 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel21 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.guna2Button33 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button34 = new Guna.UI2.WinForms.Guna2Button();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel22 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel23 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel16 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.guna2Button27 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button28 = new Guna.UI2.WinForms.Guna2Button();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.guna2TextBox5 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel17 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.guna2Button29 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button30 = new Guna.UI2.WinForms.Guna2Button();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel18 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel19 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label31 = new System.Windows.Forms.Label();
            this.guna2TextBox1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel12 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.guna2Button22 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button23 = new Guna.UI2.WinForms.Guna2Button();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.guna2TextBox3 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientPanel13 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.guna2Button24 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button26 = new Guna.UI2.WinForms.Guna2Button();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel14 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.guna2GradientPanel15 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.viewpanel.SuspendLayout();
            this.insertpanel.SuspendLayout();
            this.deletepanel.SuspendLayout();
            this.updatepanel.SuspendLayout();
            this.updatepaneloptions.SuspendLayout();
            this.unamepanel.SuspendLayout();
            this.guna2GradientPanel9.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel6.SuspendLayout();
            this.guna2GradientPanel10.SuspendLayout();
            this.guna2GradientPanel11.SuspendLayout();
            this.guna2GradientPanel8.SuspendLayout();
            this.guna2GradientPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2DataGridView3)).BeginInit();
            this.guna2GradientPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2DataGridView2)).BeginInit();
            this.guna2GradientPanel5.SuspendLayout();
            this.guna2GradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2DataGridView1)).BeginInit();
            this.guna2GradientPanel4.SuspendLayout();
            this.guna2GradientPanel2.SuspendLayout();
            this.guna2GradientPanel1.SuspendLayout();
            this.ulnamepanel.SuspendLayout();
            this.UCONTPANEL.SuspendLayout();
            this.umailpanel.SuspendLayout();
            this.UPDATEMPANEL.SuspendLayout();
            this.genderUpanel.SuspendLayout();
            this.guna2GradientPanel28.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.guna2GradientPanel29.SuspendLayout();
            this.guna2GradientPanel30.SuspendLayout();
            this.guna2GradientPanel31.SuspendLayout();
            this.guna2GradientPanel24.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel65.SuspendLayout();
            this.guna2GradientPanel25.SuspendLayout();
            this.guna2GradientPanel26.SuspendLayout();
            this.guna2GradientPanel27.SuspendLayout();
            this.guna2GradientPanel20.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.guna2GradientPanel21.SuspendLayout();
            this.guna2GradientPanel22.SuspendLayout();
            this.guna2GradientPanel23.SuspendLayout();
            this.guna2GradientPanel16.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel47.SuspendLayout();
            this.guna2GradientPanel17.SuspendLayout();
            this.guna2GradientPanel18.SuspendLayout();
            this.guna2GradientPanel19.SuspendLayout();
            this.guna2GradientPanel12.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel38.SuspendLayout();
            this.guna2GradientPanel13.SuspendLayout();
            this.guna2GradientPanel14.SuspendLayout();
            this.guna2GradientPanel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.viewpanel);
            this.panel1.Controls.Add(this.guna2Button4);
            this.panel1.Controls.Add(this.guna2Button3);
            this.panel1.Controls.Add(this.guna2Button2);
            this.panel1.Controls.Add(this.guna2Button1);
            this.panel1.Controls.Add(this.guna2GradientPanel2);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.guna2GradientPanel1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(10, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 458);
            this.panel1.TabIndex = 0;
            // 
            // viewpanel
            // 
            this.viewpanel.Controls.Add(this.insertpanel);
            this.viewpanel.Controls.Add(this.guna2DataGridView1);
            this.viewpanel.Controls.Add(this.guna2Button5);
            this.viewpanel.Controls.Add(this.panel7);
            this.viewpanel.Controls.Add(this.panel8);
            this.viewpanel.Controls.Add(this.panel9);
            this.viewpanel.Controls.Add(this.panel10);
            this.viewpanel.Controls.Add(this.guna2GradientPanel4);
            this.viewpanel.Controls.Add(this.label6);
            this.viewpanel.Location = new System.Drawing.Point(0, 0);
            this.viewpanel.Name = "viewpanel";
            this.viewpanel.Size = new System.Drawing.Size(690, 458);
            this.viewpanel.TabIndex = 21;
            this.viewpanel.Paint += new System.Windows.Forms.PaintEventHandler(this.viewpanel_Paint);
            // 
            // insertpanel
            // 
            this.insertpanel.Controls.Add(this.deletepanel);
            this.insertpanel.Controls.Add(this.genderbox);
            this.insertpanel.Controls.Add(this.dobbox);
            this.insertpanel.Controls.Add(this.contatcbox);
            this.insertpanel.Controls.Add(this.mailbo);
            this.insertpanel.Controls.Add(this.Lnamebox);
            this.insertpanel.Controls.Add(this.fnamebox);
            this.insertpanel.Controls.Add(this.guna2Button11);
            this.insertpanel.Controls.Add(this.guna2Button6);
            this.insertpanel.Controls.Add(this.panel11);
            this.insertpanel.Controls.Add(this.panel12);
            this.insertpanel.Controls.Add(this.panel13);
            this.insertpanel.Controls.Add(this.panel14);
            this.insertpanel.Controls.Add(this.guna2GradientPanel3);
            this.insertpanel.Controls.Add(this.label7);
            this.insertpanel.Location = new System.Drawing.Point(0, 0);
            this.insertpanel.Name = "insertpanel";
            this.insertpanel.Size = new System.Drawing.Size(690, 458);
            this.insertpanel.TabIndex = 22;
            this.insertpanel.Paint += new System.Windows.Forms.PaintEventHandler(this.insertpanel_Paint);
            // 
            // deletepanel
            // 
            this.deletepanel.Controls.Add(this.updatepanel);
            this.deletepanel.Controls.Add(this.guna2DataGridView2);
            this.deletepanel.Controls.Add(this.guna2Button8);
            this.deletepanel.Controls.Add(this.guna2Button7);
            this.deletepanel.Controls.Add(this.panel15);
            this.deletepanel.Controls.Add(this.panel16);
            this.deletepanel.Controls.Add(this.panel17);
            this.deletepanel.Controls.Add(this.panel18);
            this.deletepanel.Controls.Add(this.guna2GradientPanel5);
            this.deletepanel.Controls.Add(this.label9);
            this.deletepanel.Location = new System.Drawing.Point(0, 0);
            this.deletepanel.Name = "deletepanel";
            this.deletepanel.Size = new System.Drawing.Size(690, 458);
            this.deletepanel.TabIndex = 23;
            this.deletepanel.Paint += new System.Windows.Forms.PaintEventHandler(this.deletepanel_Paint);
            // 
            // updatepanel
            // 
            this.updatepanel.Controls.Add(this.updatepaneloptions);
            this.updatepanel.Controls.Add(this.guna2DataGridView3);
            this.updatepanel.Controls.Add(this.guna2Button9);
            this.updatepanel.Controls.Add(this.guna2Button10);
            this.updatepanel.Controls.Add(this.panel19);
            this.updatepanel.Controls.Add(this.panel20);
            this.updatepanel.Controls.Add(this.panel21);
            this.updatepanel.Controls.Add(this.panel22);
            this.updatepanel.Controls.Add(this.guna2GradientPanel6);
            this.updatepanel.Controls.Add(this.label11);
            this.updatepanel.Location = new System.Drawing.Point(0, 0);
            this.updatepanel.Name = "updatepanel";
            this.updatepanel.Size = new System.Drawing.Size(690, 458);
            this.updatepanel.TabIndex = 24;
            // 
            // updatepaneloptions
            // 
            this.updatepaneloptions.Controls.Add(this.unamepanel);
            this.updatepaneloptions.Controls.Add(this.guna2Button19);
            this.updatepaneloptions.Controls.Add(this.guna2Button18);
            this.updatepaneloptions.Controls.Add(this.guna2Button17);
            this.updatepaneloptions.Controls.Add(this.guna2Button16);
            this.updatepaneloptions.Controls.Add(this.guna2Button15);
            this.updatepaneloptions.Controls.Add(this.guna2Button14);
            this.updatepaneloptions.Controls.Add(this.guna2Button13);
            this.updatepaneloptions.Controls.Add(this.panel23);
            this.updatepaneloptions.Controls.Add(this.panel24);
            this.updatepaneloptions.Controls.Add(this.panel25);
            this.updatepaneloptions.Controls.Add(this.panel26);
            this.updatepaneloptions.Controls.Add(this.guna2GradientPanel7);
            this.updatepaneloptions.Controls.Add(this.label13);
            this.updatepaneloptions.Location = new System.Drawing.Point(0, 0);
            this.updatepaneloptions.Name = "updatepaneloptions";
            this.updatepaneloptions.Size = new System.Drawing.Size(690, 458);
            this.updatepaneloptions.TabIndex = 28;
            this.updatepaneloptions.Paint += new System.Windows.Forms.PaintEventHandler(this.updatepaneloptions_Paint);
            // 
            // unamepanel
            // 
            this.unamepanel.Controls.Add(this.updatefnamebox);
            this.unamepanel.Controls.Add(this.guna2GradientPanel9);
            this.unamepanel.Controls.Add(this.guna2Button12);
            this.unamepanel.Controls.Add(this.guna2Button25);
            this.unamepanel.Controls.Add(this.panel27);
            this.unamepanel.Controls.Add(this.panel28);
            this.unamepanel.Controls.Add(this.panel29);
            this.unamepanel.Controls.Add(this.panel30);
            this.unamepanel.Controls.Add(this.guna2GradientPanel8);
            this.unamepanel.Location = new System.Drawing.Point(1, 0);
            this.unamepanel.Name = "unamepanel";
            this.unamepanel.Size = new System.Drawing.Size(690, 458);
            this.unamepanel.TabIndex = 29;
            // 
            // updatefnamebox
            // 
            this.updatefnamebox.BorderColor = System.Drawing.Color.Black;
            this.updatefnamebox.BorderRadius = 8;
            this.updatefnamebox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.updatefnamebox.DefaultText = "";
            this.updatefnamebox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.updatefnamebox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.updatefnamebox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.updatefnamebox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.updatefnamebox.FillColor = System.Drawing.Color.Gainsboro;
            this.updatefnamebox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.updatefnamebox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.updatefnamebox.ForeColor = System.Drawing.Color.Black;
            this.updatefnamebox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.updatefnamebox.Location = new System.Drawing.Point(184, 221);
            this.updatefnamebox.Name = "updatefnamebox";
            this.updatefnamebox.PasswordChar = '\0';
            this.updatefnamebox.PlaceholderForeColor = System.Drawing.Color.Black;
            this.updatefnamebox.PlaceholderText = "Enter New First Name";
            this.updatefnamebox.SelectedText = "";
            this.updatefnamebox.Size = new System.Drawing.Size(337, 38);
            this.updatefnamebox.TabIndex = 27;
            // 
            // guna2GradientPanel9
            // 
            this.guna2GradientPanel9.BorderRadius = 8;
            this.guna2GradientPanel9.Controls.Add(this.label16);
            this.guna2GradientPanel9.Controls.Add(this.label15);
            this.guna2GradientPanel9.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel9.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel9.Location = new System.Drawing.Point(183, 158);
            this.guna2GradientPanel9.Name = "guna2GradientPanel9";
            this.guna2GradientPanel9.Size = new System.Drawing.Size(337, 38);
            this.guna2GradientPanel9.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(190, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 21);
            this.label16.TabIndex = 2;
            this.label16.Text = "label16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(7, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(184, 20);
            this.label15.TabIndex = 1;
            this.label15.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button12
            // 
            this.guna2Button12.AutoRoundedCorners = true;
            this.guna2Button12.BorderRadius = 14;
            this.guna2Button12.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button12.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button12.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button12.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button12.FillColor = System.Drawing.Color.Black;
            this.guna2Button12.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button12.ForeColor = System.Drawing.Color.White;
            this.guna2Button12.Location = new System.Drawing.Point(498, 339);
            this.guna2Button12.Name = "guna2Button12";
            this.guna2Button12.Size = new System.Drawing.Size(137, 31);
            this.guna2Button12.TabIndex = 18;
            this.guna2Button12.Text = "UPDATE FIRST NAME";
            this.guna2Button12.Click += new System.EventHandler(this.guna2Button12_Click);
            // 
            // guna2Button25
            // 
            this.guna2Button25.AutoRoundedCorners = true;
            this.guna2Button25.BorderRadius = 14;
            this.guna2Button25.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button25.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button25.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button25.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button25.FillColor = System.Drawing.Color.Black;
            this.guna2Button25.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button25.ForeColor = System.Drawing.Color.White;
            this.guna2Button25.Location = new System.Drawing.Point(91, 339);
            this.guna2Button25.Name = "guna2Button25";
            this.guna2Button25.Size = new System.Drawing.Size(74, 31);
            this.guna2Button25.TabIndex = 17;
            this.guna2Button25.Text = "BACK";
            this.guna2Button25.Click += new System.EventHandler(this.guna2Button25_Click);
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.Black;
            this.panel27.Location = new System.Drawing.Point(670, 28);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(10, 411);
            this.panel27.TabIndex = 16;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.Black;
            this.panel28.Controls.Add(this.panel6);
            this.panel28.Location = new System.Drawing.Point(9, 22);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(10, 411);
            this.panel28.TabIndex = 14;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.guna2TextBox2);
            this.panel6.Controls.Add(this.guna2GradientPanel10);
            this.panel6.Controls.Add(this.guna2Button20);
            this.panel6.Controls.Add(this.guna2Button21);
            this.panel6.Controls.Add(this.panel31);
            this.panel6.Controls.Add(this.panel32);
            this.panel6.Controls.Add(this.panel33);
            this.panel6.Controls.Add(this.panel34);
            this.panel6.Controls.Add(this.guna2GradientPanel11);
            this.panel6.Location = new System.Drawing.Point(0, 249);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(690, 458);
            this.panel6.TabIndex = 30;
            // 
            // guna2TextBox2
            // 
            this.guna2TextBox2.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox2.BorderRadius = 8;
            this.guna2TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox2.DefaultText = "";
            this.guna2TextBox2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox2.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.Location = new System.Drawing.Point(203, 221);
            this.guna2TextBox2.Name = "guna2TextBox2";
            this.guna2TextBox2.PasswordChar = '\0';
            this.guna2TextBox2.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox2.PlaceholderText = "Enter New First Name";
            this.guna2TextBox2.SelectedText = "";
            this.guna2TextBox2.Size = new System.Drawing.Size(309, 38);
            this.guna2TextBox2.TabIndex = 27;
            // 
            // guna2GradientPanel10
            // 
            this.guna2GradientPanel10.BorderRadius = 8;
            this.guna2GradientPanel10.Controls.Add(this.label17);
            this.guna2GradientPanel10.Controls.Add(this.label18);
            this.guna2GradientPanel10.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel10.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel10.Location = new System.Drawing.Point(202, 158);
            this.guna2GradientPanel10.Name = "guna2GradientPanel10";
            this.guna2GradientPanel10.Size = new System.Drawing.Size(309, 38);
            this.guna2GradientPanel10.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(221, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 21);
            this.label17.TabIndex = 2;
            this.label17.Text = "label17";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(7, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(184, 20);
            this.label18.TabIndex = 1;
            this.label18.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button20
            // 
            this.guna2Button20.AutoRoundedCorners = true;
            this.guna2Button20.BorderRadius = 14;
            this.guna2Button20.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button20.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button20.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button20.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button20.FillColor = System.Drawing.Color.Black;
            this.guna2Button20.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button20.ForeColor = System.Drawing.Color.White;
            this.guna2Button20.Location = new System.Drawing.Point(498, 339);
            this.guna2Button20.Name = "guna2Button20";
            this.guna2Button20.Size = new System.Drawing.Size(137, 31);
            this.guna2Button20.TabIndex = 18;
            this.guna2Button20.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button21
            // 
            this.guna2Button21.AutoRoundedCorners = true;
            this.guna2Button21.BorderRadius = 14;
            this.guna2Button21.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button21.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button21.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button21.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button21.FillColor = System.Drawing.Color.Black;
            this.guna2Button21.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button21.ForeColor = System.Drawing.Color.White;
            this.guna2Button21.Location = new System.Drawing.Point(91, 339);
            this.guna2Button21.Name = "guna2Button21";
            this.guna2Button21.Size = new System.Drawing.Size(74, 31);
            this.guna2Button21.TabIndex = 17;
            this.guna2Button21.Text = "BACK";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.Black;
            this.panel31.Location = new System.Drawing.Point(670, 28);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(10, 411);
            this.panel31.TabIndex = 16;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Black;
            this.panel32.Location = new System.Drawing.Point(9, 22);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(10, 411);
            this.panel32.TabIndex = 14;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.Black;
            this.panel33.Location = new System.Drawing.Point(9, 430);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(670, 10);
            this.panel33.TabIndex = 15;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.Black;
            this.panel34.Location = new System.Drawing.Point(10, 19);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(670, 10);
            this.panel34.TabIndex = 13;
            // 
            // guna2GradientPanel11
            // 
            this.guna2GradientPanel11.BorderRadius = 20;
            this.guna2GradientPanel11.Controls.Add(this.label19);
            this.guna2GradientPanel11.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel11.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel11.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel11.Name = "guna2GradientPanel11";
            this.guna2GradientPanel11.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel11.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Black;
            this.label19.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(7, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(166, 20);
            this.label19.TabIndex = 1;
            this.label19.Text = "UPDATE FIRST NAME";
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.Black;
            this.panel29.Location = new System.Drawing.Point(9, 430);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(670, 10);
            this.panel29.TabIndex = 15;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.Black;
            this.panel30.Location = new System.Drawing.Point(10, 19);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(670, 10);
            this.panel30.TabIndex = 13;
            // 
            // guna2GradientPanel8
            // 
            this.guna2GradientPanel8.BorderRadius = 20;
            this.guna2GradientPanel8.Controls.Add(this.label14);
            this.guna2GradientPanel8.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel8.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel8.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel8.Name = "guna2GradientPanel8";
            this.guna2GradientPanel8.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel8.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Black;
            this.label14.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(7, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(166, 20);
            this.label14.TabIndex = 1;
            this.label14.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button19
            // 
            this.guna2Button19.AutoRoundedCorners = true;
            this.guna2Button19.BorderRadius = 15;
            this.guna2Button19.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button19.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button19.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button19.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button19.FillColor = System.Drawing.Color.Black;
            this.guna2Button19.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button19.ForeColor = System.Drawing.Color.White;
            this.guna2Button19.Location = new System.Drawing.Point(119, 270);
            this.guna2Button19.Name = "guna2Button19";
            this.guna2Button19.Size = new System.Drawing.Size(155, 32);
            this.guna2Button19.TabIndex = 24;
            this.guna2Button19.Text = "UPDATE DOB";
            this.guna2Button19.Click += new System.EventHandler(this.guna2Button19_Click);
            // 
            // guna2Button18
            // 
            this.guna2Button18.AutoRoundedCorners = true;
            this.guna2Button18.BorderRadius = 15;
            this.guna2Button18.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button18.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button18.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button18.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button18.FillColor = System.Drawing.Color.Black;
            this.guna2Button18.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button18.ForeColor = System.Drawing.Color.White;
            this.guna2Button18.Location = new System.Drawing.Point(119, 308);
            this.guna2Button18.Name = "guna2Button18";
            this.guna2Button18.Size = new System.Drawing.Size(155, 32);
            this.guna2Button18.TabIndex = 23;
            this.guna2Button18.Text = "UPDATE GENDER";
            this.guna2Button18.Click += new System.EventHandler(this.guna2Button18_Click);
            // 
            // guna2Button17
            // 
            this.guna2Button17.AutoRoundedCorners = true;
            this.guna2Button17.BorderRadius = 15;
            this.guna2Button17.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button17.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button17.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button17.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button17.FillColor = System.Drawing.Color.Black;
            this.guna2Button17.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button17.ForeColor = System.Drawing.Color.White;
            this.guna2Button17.Location = new System.Drawing.Point(119, 233);
            this.guna2Button17.Name = "guna2Button17";
            this.guna2Button17.Size = new System.Drawing.Size(155, 32);
            this.guna2Button17.TabIndex = 22;
            this.guna2Button17.Text = "UPDATE EMAIL";
            this.guna2Button17.Click += new System.EventHandler(this.guna2Button17_Click);
            // 
            // guna2Button16
            // 
            this.guna2Button16.AutoRoundedCorners = true;
            this.guna2Button16.BorderRadius = 15;
            this.guna2Button16.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button16.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button16.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button16.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button16.FillColor = System.Drawing.Color.Black;
            this.guna2Button16.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button16.ForeColor = System.Drawing.Color.White;
            this.guna2Button16.Location = new System.Drawing.Point(119, 196);
            this.guna2Button16.Name = "guna2Button16";
            this.guna2Button16.Size = new System.Drawing.Size(155, 32);
            this.guna2Button16.TabIndex = 21;
            this.guna2Button16.Text = "UPDATE CONTACT";
            this.guna2Button16.Click += new System.EventHandler(this.guna2Button16_Click);
            // 
            // guna2Button15
            // 
            this.guna2Button15.AutoRoundedCorners = true;
            this.guna2Button15.BorderRadius = 15;
            this.guna2Button15.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button15.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button15.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button15.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button15.FillColor = System.Drawing.Color.Black;
            this.guna2Button15.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button15.ForeColor = System.Drawing.Color.White;
            this.guna2Button15.Location = new System.Drawing.Point(119, 158);
            this.guna2Button15.Name = "guna2Button15";
            this.guna2Button15.Size = new System.Drawing.Size(155, 32);
            this.guna2Button15.TabIndex = 20;
            this.guna2Button15.Text = "UPDATE LAST NAME";
            this.guna2Button15.Click += new System.EventHandler(this.guna2Button15_Click);
            // 
            // guna2Button14
            // 
            this.guna2Button14.AutoRoundedCorners = true;
            this.guna2Button14.BorderRadius = 15;
            this.guna2Button14.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button14.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button14.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button14.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button14.FillColor = System.Drawing.Color.Black;
            this.guna2Button14.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button14.ForeColor = System.Drawing.Color.White;
            this.guna2Button14.Location = new System.Drawing.Point(119, 120);
            this.guna2Button14.Name = "guna2Button14";
            this.guna2Button14.Size = new System.Drawing.Size(155, 32);
            this.guna2Button14.TabIndex = 19;
            this.guna2Button14.Text = "UPDATE FIRST NAME";
            this.guna2Button14.Click += new System.EventHandler(this.guna2Button14_Click);
            // 
            // guna2Button13
            // 
            this.guna2Button13.AutoRoundedCorners = true;
            this.guna2Button13.BorderRadius = 14;
            this.guna2Button13.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button13.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button13.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button13.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button13.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button13.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button13.ForeColor = System.Drawing.Color.White;
            this.guna2Button13.Location = new System.Drawing.Point(530, 357);
            this.guna2Button13.Name = "guna2Button13";
            this.guna2Button13.Size = new System.Drawing.Size(74, 31);
            this.guna2Button13.TabIndex = 17;
            this.guna2Button13.Text = "BACK";
            this.guna2Button13.Click += new System.EventHandler(this.guna2Button13_Click);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Black;
            this.panel23.Location = new System.Drawing.Point(670, 28);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(10, 411);
            this.panel23.TabIndex = 16;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Black;
            this.panel24.Location = new System.Drawing.Point(9, 22);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(10, 411);
            this.panel24.TabIndex = 14;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Black;
            this.panel25.Location = new System.Drawing.Point(9, 430);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(670, 10);
            this.panel25.TabIndex = 15;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Black;
            this.panel26.Location = new System.Drawing.Point(10, 19);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(670, 10);
            this.panel26.TabIndex = 13;
            // 
            // guna2GradientPanel7
            // 
            this.guna2GradientPanel7.BorderRadius = 20;
            this.guna2GradientPanel7.Controls.Add(this.label12);
            this.guna2GradientPanel7.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel7.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel7.Location = new System.Drawing.Point(274, 51);
            this.guna2GradientPanel7.Name = "guna2GradientPanel7";
            this.guna2GradientPanel7.Size = new System.Drawing.Size(155, 38);
            this.guna2GradientPanel7.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Black;
            this.label12.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(7, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(142, 20);
            this.label12.TabIndex = 1;
            this.label12.Text = "UPDATE OPTIONS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(294, 137);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 13);
            this.label13.TabIndex = 11;
            // 
            // guna2DataGridView3
            // 
            this.guna2DataGridView3.AllowUserToAddRows = false;
            this.guna2DataGridView3.AllowUserToDeleteRows = false;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            this.guna2DataGridView3.BackgroundColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.guna2DataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.guna2DataGridView3.ColumnHeadersHeight = 40;
            this.guna2DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.guna2DataGridView3.DefaultCellStyle = dataGridViewCellStyle39;
            this.guna2DataGridView3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2DataGridView3.Location = new System.Drawing.Point(26, 131);
            this.guna2DataGridView3.Name = "guna2DataGridView3";
            this.guna2DataGridView3.ReadOnly = true;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.guna2DataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.guna2DataGridView3.RowHeadersVisible = false;
            this.guna2DataGridView3.Size = new System.Drawing.Size(639, 196);
            this.guna2DataGridView3.TabIndex = 27;
            this.guna2DataGridView3.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView3.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.guna2DataGridView3.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.guna2DataGridView3.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.guna2DataGridView3.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.guna2DataGridView3.ThemeStyle.BackColor = System.Drawing.Color.Black;
            this.guna2DataGridView3.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2DataGridView3.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Black;
            this.guna2DataGridView3.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.guna2DataGridView3.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2DataGridView3.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.guna2DataGridView3.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.guna2DataGridView3.ThemeStyle.HeaderStyle.Height = 40;
            this.guna2DataGridView3.ThemeStyle.ReadOnly = true;
            this.guna2DataGridView3.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView3.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.guna2DataGridView3.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2DataGridView3.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.guna2DataGridView3.ThemeStyle.RowsStyle.Height = 22;
            this.guna2DataGridView3.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.guna2DataGridView3.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // guna2Button9
            // 
            this.guna2Button9.AutoRoundedCorners = true;
            this.guna2Button9.BorderRadius = 14;
            this.guna2Button9.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button9.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button9.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button9.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button9.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button9.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button9.ForeColor = System.Drawing.Color.White;
            this.guna2Button9.Location = new System.Drawing.Point(530, 362);
            this.guna2Button9.Name = "guna2Button9";
            this.guna2Button9.Size = new System.Drawing.Size(74, 31);
            this.guna2Button9.TabIndex = 18;
            this.guna2Button9.Text = "UPDATE";
            this.guna2Button9.Click += new System.EventHandler(this.guna2Button9_Click);
            // 
            // guna2Button10
            // 
            this.guna2Button10.AutoRoundedCorners = true;
            this.guna2Button10.BorderRadius = 14;
            this.guna2Button10.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button10.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button10.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button10.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button10.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button10.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button10.ForeColor = System.Drawing.Color.White;
            this.guna2Button10.Location = new System.Drawing.Point(91, 359);
            this.guna2Button10.Name = "guna2Button10";
            this.guna2Button10.Size = new System.Drawing.Size(74, 31);
            this.guna2Button10.TabIndex = 17;
            this.guna2Button10.Text = "BACK";
            this.guna2Button10.Click += new System.EventHandler(this.guna2Button10_Click);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Black;
            this.panel19.Location = new System.Drawing.Point(670, 28);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(10, 411);
            this.panel19.TabIndex = 16;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Black;
            this.panel20.Location = new System.Drawing.Point(9, 22);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(10, 411);
            this.panel20.TabIndex = 14;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Black;
            this.panel21.Location = new System.Drawing.Point(9, 430);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(670, 10);
            this.panel21.TabIndex = 15;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Black;
            this.panel22.Location = new System.Drawing.Point(10, 19);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(670, 10);
            this.panel22.TabIndex = 13;
            // 
            // guna2GradientPanel6
            // 
            this.guna2GradientPanel6.BorderRadius = 20;
            this.guna2GradientPanel6.Controls.Add(this.label10);
            this.guna2GradientPanel6.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel6.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel6.Location = new System.Drawing.Point(275, 64);
            this.guna2GradientPanel6.Name = "guna2GradientPanel6";
            this.guna2GradientPanel6.Size = new System.Drawing.Size(155, 38);
            this.guna2GradientPanel6.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Black;
            this.label10.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "UPDATE STUDENT";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(294, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 11;
            // 
            // guna2DataGridView2
            // 
            this.guna2DataGridView2.AllowUserToAddRows = false;
            this.guna2DataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle41;
            this.guna2DataGridView2.BackgroundColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.guna2DataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.guna2DataGridView2.ColumnHeadersHeight = 4;
            this.guna2DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.guna2DataGridView2.DefaultCellStyle = dataGridViewCellStyle43;
            this.guna2DataGridView2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2DataGridView2.Location = new System.Drawing.Point(26, 131);
            this.guna2DataGridView2.Name = "guna2DataGridView2";
            this.guna2DataGridView2.ReadOnly = true;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.guna2DataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle44;
            this.guna2DataGridView2.RowHeadersVisible = false;
            this.guna2DataGridView2.Size = new System.Drawing.Size(639, 196);
            this.guna2DataGridView2.TabIndex = 27;
            this.guna2DataGridView2.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView2.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.guna2DataGridView2.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.guna2DataGridView2.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.guna2DataGridView2.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.guna2DataGridView2.ThemeStyle.BackColor = System.Drawing.Color.Black;
            this.guna2DataGridView2.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2DataGridView2.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Black;
            this.guna2DataGridView2.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.guna2DataGridView2.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2DataGridView2.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.guna2DataGridView2.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.guna2DataGridView2.ThemeStyle.HeaderStyle.Height = 4;
            this.guna2DataGridView2.ThemeStyle.ReadOnly = true;
            this.guna2DataGridView2.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView2.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.guna2DataGridView2.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2DataGridView2.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.guna2DataGridView2.ThemeStyle.RowsStyle.Height = 22;
            this.guna2DataGridView2.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.guna2DataGridView2.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // guna2Button8
            // 
            this.guna2Button8.AutoRoundedCorners = true;
            this.guna2Button8.BorderRadius = 14;
            this.guna2Button8.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button8.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button8.ForeColor = System.Drawing.Color.White;
            this.guna2Button8.Location = new System.Drawing.Point(530, 362);
            this.guna2Button8.Name = "guna2Button8";
            this.guna2Button8.Size = new System.Drawing.Size(74, 31);
            this.guna2Button8.TabIndex = 18;
            this.guna2Button8.Text = "DELETE";
            this.guna2Button8.Click += new System.EventHandler(this.guna2Button8_Click);
            // 
            // guna2Button7
            // 
            this.guna2Button7.AutoRoundedCorners = true;
            this.guna2Button7.BorderRadius = 14;
            this.guna2Button7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button7.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button7.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button7.ForeColor = System.Drawing.Color.White;
            this.guna2Button7.Location = new System.Drawing.Point(91, 359);
            this.guna2Button7.Name = "guna2Button7";
            this.guna2Button7.Size = new System.Drawing.Size(74, 31);
            this.guna2Button7.TabIndex = 17;
            this.guna2Button7.Text = "BACK";
            this.guna2Button7.Click += new System.EventHandler(this.guna2Button7_Click);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Black;
            this.panel15.Location = new System.Drawing.Point(670, 28);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(10, 411);
            this.panel15.TabIndex = 16;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Black;
            this.panel16.Location = new System.Drawing.Point(9, 22);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(10, 411);
            this.panel16.TabIndex = 14;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Black;
            this.panel17.Location = new System.Drawing.Point(9, 430);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(670, 10);
            this.panel17.TabIndex = 15;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Black;
            this.panel18.Location = new System.Drawing.Point(10, 19);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(670, 10);
            this.panel18.TabIndex = 13;
            // 
            // guna2GradientPanel5
            // 
            this.guna2GradientPanel5.BorderRadius = 20;
            this.guna2GradientPanel5.Controls.Add(this.label8);
            this.guna2GradientPanel5.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel5.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel5.Location = new System.Drawing.Point(275, 64);
            this.guna2GradientPanel5.Name = "guna2GradientPanel5";
            this.guna2GradientPanel5.Size = new System.Drawing.Size(155, 38);
            this.guna2GradientPanel5.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(10, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 20);
            this.label8.TabIndex = 1;
            this.label8.Text = "DELETE STUDENT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(294, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 11;
            // 
            // genderbox
            // 
            this.genderbox.BackColor = System.Drawing.Color.Transparent;
            this.genderbox.BorderColor = System.Drawing.Color.Black;
            this.genderbox.BorderRadius = 8;
            this.genderbox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.genderbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genderbox.FillColor = System.Drawing.Color.Gainsboro;
            this.genderbox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.genderbox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.genderbox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.genderbox.ForeColor = System.Drawing.Color.Black;
            this.genderbox.ItemHeight = 30;
            this.genderbox.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.genderbox.Location = new System.Drawing.Point(402, 255);
            this.genderbox.Name = "genderbox";
            this.genderbox.Size = new System.Drawing.Size(199, 36);
            this.genderbox.TabIndex = 31;
            // 
            // dobbox
            // 
            this.dobbox.BorderRadius = 8;
            this.dobbox.Checked = true;
            this.dobbox.FillColor = System.Drawing.Color.Gainsboro;
            this.dobbox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dobbox.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dobbox.Location = new System.Drawing.Point(105, 255);
            this.dobbox.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dobbox.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dobbox.Name = "dobbox";
            this.dobbox.Size = new System.Drawing.Size(199, 41);
            this.dobbox.TabIndex = 30;
            this.dobbox.Value = new System.DateTime(2024, 3, 6, 11, 37, 24, 950);
            // 
            // contatcbox
            // 
            this.contatcbox.BorderColor = System.Drawing.Color.Black;
            this.contatcbox.BorderRadius = 8;
            this.contatcbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.contatcbox.DefaultText = "";
            this.contatcbox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.contatcbox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.contatcbox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.contatcbox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.contatcbox.FillColor = System.Drawing.Color.Gainsboro;
            this.contatcbox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.contatcbox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.contatcbox.ForeColor = System.Drawing.Color.Black;
            this.contatcbox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.contatcbox.Location = new System.Drawing.Point(105, 202);
            this.contatcbox.Name = "contatcbox";
            this.contatcbox.PasswordChar = '\0';
            this.contatcbox.PlaceholderForeColor = System.Drawing.Color.Black;
            this.contatcbox.PlaceholderText = "Enter Contact";
            this.contatcbox.SelectedText = "";
            this.contatcbox.Size = new System.Drawing.Size(199, 36);
            this.contatcbox.TabIndex = 29;
            // 
            // mailbo
            // 
            this.mailbo.BorderColor = System.Drawing.Color.Black;
            this.mailbo.BorderRadius = 8;
            this.mailbo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mailbo.DefaultText = "";
            this.mailbo.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.mailbo.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.mailbo.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.mailbo.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.mailbo.FillColor = System.Drawing.Color.Gainsboro;
            this.mailbo.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.mailbo.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mailbo.ForeColor = System.Drawing.Color.Black;
            this.mailbo.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.mailbo.Location = new System.Drawing.Point(402, 202);
            this.mailbo.Name = "mailbo";
            this.mailbo.PasswordChar = '\0';
            this.mailbo.PlaceholderForeColor = System.Drawing.Color.Black;
            this.mailbo.PlaceholderText = "Enter Email";
            this.mailbo.SelectedText = "";
            this.mailbo.Size = new System.Drawing.Size(199, 36);
            this.mailbo.TabIndex = 28;
            // 
            // Lnamebox
            // 
            this.Lnamebox.BorderColor = System.Drawing.Color.Black;
            this.Lnamebox.BorderRadius = 8;
            this.Lnamebox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Lnamebox.DefaultText = "";
            this.Lnamebox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.Lnamebox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.Lnamebox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.Lnamebox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.Lnamebox.FillColor = System.Drawing.Color.Gainsboro;
            this.Lnamebox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Lnamebox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Lnamebox.ForeColor = System.Drawing.Color.Black;
            this.Lnamebox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.Lnamebox.Location = new System.Drawing.Point(402, 153);
            this.Lnamebox.Name = "Lnamebox";
            this.Lnamebox.PasswordChar = '\0';
            this.Lnamebox.PlaceholderForeColor = System.Drawing.Color.Black;
            this.Lnamebox.PlaceholderText = "Enter Last Name";
            this.Lnamebox.SelectedText = "";
            this.Lnamebox.Size = new System.Drawing.Size(199, 36);
            this.Lnamebox.TabIndex = 27;
            // 
            // fnamebox
            // 
            this.fnamebox.BorderColor = System.Drawing.Color.Black;
            this.fnamebox.BorderRadius = 8;
            this.fnamebox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fnamebox.DefaultText = "";
            this.fnamebox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.fnamebox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.fnamebox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.fnamebox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.fnamebox.FillColor = System.Drawing.Color.Gainsboro;
            this.fnamebox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.fnamebox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.fnamebox.ForeColor = System.Drawing.Color.Black;
            this.fnamebox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.fnamebox.Location = new System.Drawing.Point(105, 153);
            this.fnamebox.Name = "fnamebox";
            this.fnamebox.PasswordChar = '\0';
            this.fnamebox.PlaceholderForeColor = System.Drawing.Color.Black;
            this.fnamebox.PlaceholderText = "Enter First Name";
            this.fnamebox.SelectedText = "";
            this.fnamebox.Size = new System.Drawing.Size(199, 36);
            this.fnamebox.TabIndex = 26;
            // 
            // guna2Button11
            // 
            this.guna2Button11.AutoRoundedCorners = true;
            this.guna2Button11.BorderRadius = 17;
            this.guna2Button11.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button11.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button11.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button11.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button11.FillColor = System.Drawing.Color.Black;
            this.guna2Button11.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button11.ForeColor = System.Drawing.Color.White;
            this.guna2Button11.Location = new System.Drawing.Point(476, 344);
            this.guna2Button11.Name = "guna2Button11";
            this.guna2Button11.Size = new System.Drawing.Size(74, 37);
            this.guna2Button11.TabIndex = 24;
            this.guna2Button11.Text = "ADD";
            this.guna2Button11.Click += new System.EventHandler(this.guna2Button11_Click);
            // 
            // guna2Button6
            // 
            this.guna2Button6.AutoRoundedCorners = true;
            this.guna2Button6.BorderRadius = 17;
            this.guna2Button6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button6.FillColor = System.Drawing.Color.Black;
            this.guna2Button6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button6.ForeColor = System.Drawing.Color.White;
            this.guna2Button6.Location = new System.Drawing.Point(150, 344);
            this.guna2Button6.Name = "guna2Button6";
            this.guna2Button6.Size = new System.Drawing.Size(74, 37);
            this.guna2Button6.TabIndex = 17;
            this.guna2Button6.Text = "BACK";
            this.guna2Button6.Click += new System.EventHandler(this.guna2Button6_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Black;
            this.panel11.Location = new System.Drawing.Point(670, 28);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(10, 411);
            this.panel11.TabIndex = 16;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Black;
            this.panel12.Location = new System.Drawing.Point(9, 22);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 411);
            this.panel12.TabIndex = 14;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Black;
            this.panel13.Location = new System.Drawing.Point(9, 430);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(670, 10);
            this.panel13.TabIndex = 15;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Black;
            this.panel14.Location = new System.Drawing.Point(10, 19);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(670, 10);
            this.panel14.TabIndex = 13;
            // 
            // guna2GradientPanel3
            // 
            this.guna2GradientPanel3.BorderRadius = 20;
            this.guna2GradientPanel3.Controls.Add(this.label4);
            this.guna2GradientPanel3.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel3.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel3.Location = new System.Drawing.Point(275, 64);
            this.guna2GradientPanel3.Name = "guna2GradientPanel3";
            this.guna2GradientPanel3.Size = new System.Drawing.Size(155, 38);
            this.guna2GradientPanel3.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(10, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "INSERT STUDENT";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(343, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 11;
            // 
            // guna2DataGridView1
            // 
            this.guna2DataGridView1.AllowUserToAddRows = false;
            this.guna2DataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle45;
            this.guna2DataGridView1.BackgroundColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle46.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.guna2DataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle46;
            this.guna2DataGridView1.ColumnHeadersHeight = 4;
            this.guna2DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.guna2DataGridView1.DefaultCellStyle = dataGridViewCellStyle47;
            this.guna2DataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2DataGridView1.Location = new System.Drawing.Point(25, 131);
            this.guna2DataGridView1.Name = "guna2DataGridView1";
            this.guna2DataGridView1.ReadOnly = true;
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.guna2DataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle48;
            this.guna2DataGridView1.RowHeadersVisible = false;
            this.guna2DataGridView1.Size = new System.Drawing.Size(639, 196);
            this.guna2DataGridView1.TabIndex = 23;
            this.guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.guna2DataGridView1.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.guna2DataGridView1.ThemeStyle.BackColor = System.Drawing.Color.Black;
            this.guna2DataGridView1.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2DataGridView1.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Black;
            this.guna2DataGridView1.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.guna2DataGridView1.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2DataGridView1.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.guna2DataGridView1.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.guna2DataGridView1.ThemeStyle.HeaderStyle.Height = 4;
            this.guna2DataGridView1.ThemeStyle.ReadOnly = true;
            this.guna2DataGridView1.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.guna2DataGridView1.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.guna2DataGridView1.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2DataGridView1.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.guna2DataGridView1.ThemeStyle.RowsStyle.Height = 22;
            this.guna2DataGridView1.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.guna2DataGridView1.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // guna2Button5
            // 
            this.guna2Button5.AutoRoundedCorners = true;
            this.guna2Button5.BorderRadius = 19;
            this.guna2Button5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button5.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button5.ForeColor = System.Drawing.Color.White;
            this.guna2Button5.Location = new System.Drawing.Point(312, 353);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.Size = new System.Drawing.Size(84, 40);
            this.guna2Button5.TabIndex = 17;
            this.guna2Button5.Text = "BACK";
            this.guna2Button5.Click += new System.EventHandler(this.guna2Button5_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.Location = new System.Drawing.Point(670, 28);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 411);
            this.panel7.TabIndex = 16;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Black;
            this.panel8.Location = new System.Drawing.Point(9, 22);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(10, 411);
            this.panel8.TabIndex = 14;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Black;
            this.panel9.Location = new System.Drawing.Point(9, 430);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(670, 10);
            this.panel9.TabIndex = 15;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Black;
            this.panel10.Location = new System.Drawing.Point(10, 19);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(670, 10);
            this.panel10.TabIndex = 13;
            // 
            // guna2GradientPanel4
            // 
            this.guna2GradientPanel4.BorderRadius = 20;
            this.guna2GradientPanel4.Controls.Add(this.label5);
            this.guna2GradientPanel4.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel4.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel4.Location = new System.Drawing.Point(275, 64);
            this.guna2GradientPanel4.Name = "guna2GradientPanel4";
            this.guna2GradientPanel4.Size = new System.Drawing.Size(155, 38);
            this.guna2GradientPanel4.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Black;
            this.label5.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(19, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "VIEW STUDENT";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(294, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 11;
            // 
            // guna2Button4
            // 
            this.guna2Button4.AutoRoundedCorners = true;
            this.guna2Button4.BorderRadius = 17;
            this.guna2Button4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button4.FillColor = System.Drawing.Color.Black;
            this.guna2Button4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.Location = new System.Drawing.Point(273, 326);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.Size = new System.Drawing.Size(166, 36);
            this.guna2Button4.TabIndex = 20;
            this.guna2Button4.Text = "UPDATE";
            this.guna2Button4.Click += new System.EventHandler(this.guna2Button4_Click);
            // 
            // guna2Button3
            // 
            this.guna2Button3.AutoRoundedCorners = true;
            this.guna2Button3.BorderRadius = 17;
            this.guna2Button3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button3.FillColor = System.Drawing.Color.Black;
            this.guna2Button3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.Location = new System.Drawing.Point(273, 279);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.Size = new System.Drawing.Size(166, 36);
            this.guna2Button3.TabIndex = 19;
            this.guna2Button3.Text = "DELETE";
            this.guna2Button3.Click += new System.EventHandler(this.guna2Button3_Click);
            // 
            // guna2Button2
            // 
            this.guna2Button2.AutoRoundedCorners = true;
            this.guna2Button2.BorderRadius = 17;
            this.guna2Button2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button2.FillColor = System.Drawing.Color.Black;
            this.guna2Button2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.Location = new System.Drawing.Point(273, 232);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.Size = new System.Drawing.Size(166, 36);
            this.guna2Button2.TabIndex = 18;
            this.guna2Button2.Text = "INSERT";
            this.guna2Button2.Click += new System.EventHandler(this.guna2Button2_Click);
            // 
            // guna2Button1
            // 
            this.guna2Button1.AutoRoundedCorners = true;
            this.guna2Button1.BorderRadius = 17;
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.Black;
            this.guna2Button1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.Location = new System.Drawing.Point(273, 185);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.Size = new System.Drawing.Size(166, 36);
            this.guna2Button1.TabIndex = 17;
            this.guna2Button1.Text = "VIEW";
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // guna2GradientPanel2
            // 
            this.guna2GradientPanel2.BorderRadius = 20;
            this.guna2GradientPanel2.Controls.Add(this.label3);
            this.guna2GradientPanel2.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel2.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel2.Location = new System.Drawing.Point(116, 134);
            this.guna2GradientPanel2.Name = "guna2GradientPanel2";
            this.guna2GradientPanel2.Size = new System.Drawing.Size(488, 36);
            this.guna2GradientPanel2.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(100, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(296, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "CHOOSE ANY ONE OF THE FOLLOWING ";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(670, 28);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 411);
            this.panel4.TabIndex = 16;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(9, 22);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 411);
            this.panel3.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(9, 430);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(670, 10);
            this.panel2.TabIndex = 15;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(10, 19);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(670, 10);
            this.panel5.TabIndex = 13;
            // 
            // guna2GradientPanel1
            // 
            this.guna2GradientPanel1.BorderRadius = 20;
            this.guna2GradientPanel1.Controls.Add(this.label2);
            this.guna2GradientPanel1.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel1.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel1.Location = new System.Drawing.Point(278, 87);
            this.guna2GradientPanel1.Name = "guna2GradientPanel1";
            this.guna2GradientPanel1.Size = new System.Drawing.Size(155, 38);
            this.guna2GradientPanel1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(39, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "STUDENT";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(294, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 11;
            // 
            // ulnamepanel
            // 
            this.ulnamepanel.Controls.Add(this.UCONTPANEL);
            this.ulnamepanel.Controls.Add(this.guna2TextBox1);
            this.ulnamepanel.Controls.Add(this.guna2GradientPanel12);
            this.ulnamepanel.Controls.Add(this.guna2Button22);
            this.ulnamepanel.Controls.Add(this.guna2Button23);
            this.ulnamepanel.Controls.Add(this.panel36);
            this.ulnamepanel.Controls.Add(this.panel37);
            this.ulnamepanel.Controls.Add(this.panel43);
            this.ulnamepanel.Controls.Add(this.panel44);
            this.ulnamepanel.Controls.Add(this.guna2GradientPanel15);
            this.ulnamepanel.Location = new System.Drawing.Point(10, 3);
            this.ulnamepanel.Name = "ulnamepanel";
            this.ulnamepanel.Size = new System.Drawing.Size(690, 458);
            this.ulnamepanel.TabIndex = 30;
            // 
            // UCONTPANEL
            // 
            this.UCONTPANEL.Controls.Add(this.umailpanel);
            this.UCONTPANEL.Controls.Add(this.guna2TextBox4);
            this.UCONTPANEL.Controls.Add(this.guna2GradientPanel16);
            this.UCONTPANEL.Controls.Add(this.guna2Button27);
            this.UCONTPANEL.Controls.Add(this.guna2Button28);
            this.UCONTPANEL.Controls.Add(this.panel45);
            this.UCONTPANEL.Controls.Add(this.panel46);
            this.UCONTPANEL.Controls.Add(this.panel52);
            this.UCONTPANEL.Controls.Add(this.panel53);
            this.UCONTPANEL.Controls.Add(this.guna2GradientPanel19);
            this.UCONTPANEL.Location = new System.Drawing.Point(0, 1);
            this.UCONTPANEL.Name = "UCONTPANEL";
            this.UCONTPANEL.Size = new System.Drawing.Size(690, 458);
            this.UCONTPANEL.TabIndex = 31;
            // 
            // umailpanel
            // 
            this.umailpanel.Controls.Add(this.UPDATEMPANEL);
            this.umailpanel.Controls.Add(this.guna2TextBox6);
            this.umailpanel.Controls.Add(this.guna2GradientPanel20);
            this.umailpanel.Controls.Add(this.guna2Button31);
            this.umailpanel.Controls.Add(this.guna2Button32);
            this.umailpanel.Controls.Add(this.panel54);
            this.umailpanel.Controls.Add(this.panel55);
            this.umailpanel.Controls.Add(this.panel61);
            this.umailpanel.Controls.Add(this.panel62);
            this.umailpanel.Controls.Add(this.guna2GradientPanel23);
            this.umailpanel.Location = new System.Drawing.Point(0, 0);
            this.umailpanel.Name = "umailpanel";
            this.umailpanel.Size = new System.Drawing.Size(690, 458);
            this.umailpanel.TabIndex = 32;
            // 
            // UPDATEMPANEL
            // 
            this.UPDATEMPANEL.Controls.Add(this.genderUpanel);
            this.UPDATEMPANEL.Controls.Add(this.guna2DateTimePicker2);
            this.UPDATEMPANEL.Controls.Add(this.guna2GradientPanel24);
            this.UPDATEMPANEL.Controls.Add(this.guna2Button35);
            this.UPDATEMPANEL.Controls.Add(this.guna2Button36);
            this.UPDATEMPANEL.Controls.Add(this.panel63);
            this.UPDATEMPANEL.Controls.Add(this.panel64);
            this.UPDATEMPANEL.Controls.Add(this.panel70);
            this.UPDATEMPANEL.Controls.Add(this.panel71);
            this.UPDATEMPANEL.Controls.Add(this.guna2GradientPanel27);
            this.UPDATEMPANEL.Location = new System.Drawing.Point(0, 0);
            this.UPDATEMPANEL.Name = "UPDATEMPANEL";
            this.UPDATEMPANEL.Size = new System.Drawing.Size(690, 458);
            this.UPDATEMPANEL.TabIndex = 33;
            // 
            // genderUpanel
            // 
            this.genderUpanel.Controls.Add(this.guna2ComboBox2);
            this.genderUpanel.Controls.Add(this.guna2GradientPanel28);
            this.genderUpanel.Controls.Add(this.guna2Button39);
            this.genderUpanel.Controls.Add(this.guna2Button40);
            this.genderUpanel.Controls.Add(this.panel72);
            this.genderUpanel.Controls.Add(this.panel73);
            this.genderUpanel.Controls.Add(this.panel79);
            this.genderUpanel.Controls.Add(this.panel80);
            this.genderUpanel.Controls.Add(this.guna2GradientPanel31);
            this.genderUpanel.Location = new System.Drawing.Point(0, 0);
            this.genderUpanel.Name = "genderUpanel";
            this.genderUpanel.Size = new System.Drawing.Size(690, 458);
            this.genderUpanel.TabIndex = 34;
            // 
            // guna2ComboBox2
            // 
            this.guna2ComboBox2.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox2.BorderColor = System.Drawing.Color.Black;
            this.guna2ComboBox2.BorderRadius = 8;
            this.guna2ComboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox2.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2ComboBox2.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox2.ForeColor = System.Drawing.Color.Black;
            this.guna2ComboBox2.ItemHeight = 30;
            this.guna2ComboBox2.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.guna2ComboBox2.Location = new System.Drawing.Point(184, 227);
            this.guna2ComboBox2.Name = "guna2ComboBox2";
            this.guna2ComboBox2.Size = new System.Drawing.Size(336, 36);
            this.guna2ComboBox2.TabIndex = 20;
            // 
            // guna2GradientPanel28
            // 
            this.guna2GradientPanel28.BorderRadius = 8;
            this.guna2GradientPanel28.Controls.Add(this.label44);
            this.guna2GradientPanel28.Controls.Add(this.label45);
            this.guna2GradientPanel28.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel28.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel28.Location = new System.Drawing.Point(185, 158);
            this.guna2GradientPanel28.Name = "guna2GradientPanel28";
            this.guna2GradientPanel28.Size = new System.Drawing.Size(337, 38);
            this.guna2GradientPanel28.TabIndex = 13;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(161, 8);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 21);
            this.label44.TabIndex = 2;
            this.label44.Text = "label44";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(7, 8);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(152, 20);
            this.label45.TabIndex = 1;
            this.label45.Text = "CURRENT GENDER :";
            // 
            // guna2Button39
            // 
            this.guna2Button39.AutoRoundedCorners = true;
            this.guna2Button39.BorderRadius = 14;
            this.guna2Button39.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button39.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button39.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button39.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button39.FillColor = System.Drawing.Color.Black;
            this.guna2Button39.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button39.ForeColor = System.Drawing.Color.White;
            this.guna2Button39.Location = new System.Drawing.Point(498, 339);
            this.guna2Button39.Name = "guna2Button39";
            this.guna2Button39.Size = new System.Drawing.Size(137, 31);
            this.guna2Button39.TabIndex = 18;
            this.guna2Button39.Text = "UPDATE GENDER";
            this.guna2Button39.Click += new System.EventHandler(this.guna2Button39_Click);
            // 
            // guna2Button40
            // 
            this.guna2Button40.AutoRoundedCorners = true;
            this.guna2Button40.BorderRadius = 14;
            this.guna2Button40.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button40.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button40.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button40.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button40.FillColor = System.Drawing.Color.Black;
            this.guna2Button40.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button40.ForeColor = System.Drawing.Color.White;
            this.guna2Button40.Location = new System.Drawing.Point(91, 339);
            this.guna2Button40.Name = "guna2Button40";
            this.guna2Button40.Size = new System.Drawing.Size(74, 31);
            this.guna2Button40.TabIndex = 17;
            this.guna2Button40.Text = "BACK";
            this.guna2Button40.Click += new System.EventHandler(this.guna2Button40_Click);
            // 
            // panel72
            // 
            this.panel72.BackColor = System.Drawing.Color.Black;
            this.panel72.Location = new System.Drawing.Point(670, 28);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(10, 411);
            this.panel72.TabIndex = 16;
            // 
            // panel73
            // 
            this.panel73.BackColor = System.Drawing.Color.Black;
            this.panel73.Controls.Add(this.panel74);
            this.panel73.Location = new System.Drawing.Point(9, 22);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(10, 411);
            this.panel73.TabIndex = 14;
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.guna2TextBox8);
            this.panel74.Controls.Add(this.guna2GradientPanel29);
            this.panel74.Controls.Add(this.guna2Button41);
            this.panel74.Controls.Add(this.guna2Button42);
            this.panel74.Controls.Add(this.panel75);
            this.panel74.Controls.Add(this.panel76);
            this.panel74.Controls.Add(this.panel77);
            this.panel74.Controls.Add(this.panel78);
            this.panel74.Controls.Add(this.guna2GradientPanel30);
            this.panel74.Location = new System.Drawing.Point(0, 249);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(690, 458);
            this.panel74.TabIndex = 30;
            // 
            // guna2TextBox8
            // 
            this.guna2TextBox8.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox8.BorderRadius = 8;
            this.guna2TextBox8.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox8.DefaultText = "";
            this.guna2TextBox8.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox8.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox8.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox8.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox8.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox8.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox8.Location = new System.Drawing.Point(203, 221);
            this.guna2TextBox8.Name = "guna2TextBox8";
            this.guna2TextBox8.PasswordChar = '\0';
            this.guna2TextBox8.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox8.PlaceholderText = "Enter New First Name";
            this.guna2TextBox8.SelectedText = "";
            this.guna2TextBox8.Size = new System.Drawing.Size(309, 38);
            this.guna2TextBox8.TabIndex = 27;
            // 
            // guna2GradientPanel29
            // 
            this.guna2GradientPanel29.BorderRadius = 8;
            this.guna2GradientPanel29.Controls.Add(this.label46);
            this.guna2GradientPanel29.Controls.Add(this.label47);
            this.guna2GradientPanel29.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel29.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel29.Location = new System.Drawing.Point(202, 158);
            this.guna2GradientPanel29.Name = "guna2GradientPanel29";
            this.guna2GradientPanel29.Size = new System.Drawing.Size(309, 38);
            this.guna2GradientPanel29.TabIndex = 13;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(221, 8);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(68, 21);
            this.label46.TabIndex = 2;
            this.label46.Text = "label46";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(7, 8);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(184, 20);
            this.label47.TabIndex = 1;
            this.label47.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button41
            // 
            this.guna2Button41.AutoRoundedCorners = true;
            this.guna2Button41.BorderRadius = 14;
            this.guna2Button41.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button41.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button41.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button41.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button41.FillColor = System.Drawing.Color.Black;
            this.guna2Button41.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button41.ForeColor = System.Drawing.Color.White;
            this.guna2Button41.Location = new System.Drawing.Point(498, 339);
            this.guna2Button41.Name = "guna2Button41";
            this.guna2Button41.Size = new System.Drawing.Size(137, 31);
            this.guna2Button41.TabIndex = 18;
            this.guna2Button41.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button42
            // 
            this.guna2Button42.AutoRoundedCorners = true;
            this.guna2Button42.BorderRadius = 14;
            this.guna2Button42.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button42.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button42.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button42.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button42.FillColor = System.Drawing.Color.Black;
            this.guna2Button42.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button42.ForeColor = System.Drawing.Color.White;
            this.guna2Button42.Location = new System.Drawing.Point(91, 339);
            this.guna2Button42.Name = "guna2Button42";
            this.guna2Button42.Size = new System.Drawing.Size(74, 31);
            this.guna2Button42.TabIndex = 17;
            this.guna2Button42.Text = "BACK";
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.Black;
            this.panel75.Location = new System.Drawing.Point(670, 28);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(10, 411);
            this.panel75.TabIndex = 16;
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.Color.Black;
            this.panel76.Location = new System.Drawing.Point(9, 22);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(10, 411);
            this.panel76.TabIndex = 14;
            // 
            // panel77
            // 
            this.panel77.BackColor = System.Drawing.Color.Black;
            this.panel77.Location = new System.Drawing.Point(9, 430);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(670, 10);
            this.panel77.TabIndex = 15;
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.Color.Black;
            this.panel78.Location = new System.Drawing.Point(10, 19);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(670, 10);
            this.panel78.TabIndex = 13;
            // 
            // guna2GradientPanel30
            // 
            this.guna2GradientPanel30.BorderRadius = 20;
            this.guna2GradientPanel30.Controls.Add(this.label48);
            this.guna2GradientPanel30.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel30.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel30.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel30.Name = "guna2GradientPanel30";
            this.guna2GradientPanel30.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel30.TabIndex = 12;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Black;
            this.label48.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(7, 8);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(166, 20);
            this.label48.TabIndex = 1;
            this.label48.Text = "UPDATE FIRST NAME";
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.Black;
            this.panel79.Location = new System.Drawing.Point(9, 430);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(670, 10);
            this.panel79.TabIndex = 15;
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.Black;
            this.panel80.Location = new System.Drawing.Point(10, 19);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(670, 10);
            this.panel80.TabIndex = 13;
            // 
            // guna2GradientPanel31
            // 
            this.guna2GradientPanel31.BorderRadius = 20;
            this.guna2GradientPanel31.Controls.Add(this.label49);
            this.guna2GradientPanel31.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel31.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel31.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel31.Name = "guna2GradientPanel31";
            this.guna2GradientPanel31.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel31.TabIndex = 12;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Black;
            this.label49.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(31, 8);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(134, 20);
            this.label49.TabIndex = 1;
            this.label49.Text = "UPDATE GENDER";
            // 
            // guna2DateTimePicker2
            // 
            this.guna2DateTimePicker2.BorderRadius = 8;
            this.guna2DateTimePicker2.Checked = true;
            this.guna2DateTimePicker2.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2DateTimePicker2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.guna2DateTimePicker2.Location = new System.Drawing.Point(186, 222);
            this.guna2DateTimePicker2.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker2.Name = "guna2DateTimePicker2";
            this.guna2DateTimePicker2.Size = new System.Drawing.Size(337, 36);
            this.guna2DateTimePicker2.TabIndex = 20;
            this.guna2DateTimePicker2.Value = new System.DateTime(2024, 3, 6, 16, 22, 13, 794);
            // 
            // guna2GradientPanel24
            // 
            this.guna2GradientPanel24.BorderRadius = 8;
            this.guna2GradientPanel24.Controls.Add(this.label38);
            this.guna2GradientPanel24.Controls.Add(this.label39);
            this.guna2GradientPanel24.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel24.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel24.Location = new System.Drawing.Point(185, 158);
            this.guna2GradientPanel24.Name = "guna2GradientPanel24";
            this.guna2GradientPanel24.Size = new System.Drawing.Size(337, 38);
            this.guna2GradientPanel24.TabIndex = 13;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(126, 8);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(68, 21);
            this.label38.TabIndex = 2;
            this.label38.Text = "label38";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(7, 8);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(124, 20);
            this.label39.TabIndex = 1;
            this.label39.Text = "CURRENT DOB :";
            // 
            // guna2Button35
            // 
            this.guna2Button35.AutoRoundedCorners = true;
            this.guna2Button35.BorderRadius = 14;
            this.guna2Button35.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button35.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button35.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button35.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button35.FillColor = System.Drawing.Color.Black;
            this.guna2Button35.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button35.ForeColor = System.Drawing.Color.White;
            this.guna2Button35.Location = new System.Drawing.Point(498, 339);
            this.guna2Button35.Name = "guna2Button35";
            this.guna2Button35.Size = new System.Drawing.Size(137, 31);
            this.guna2Button35.TabIndex = 18;
            this.guna2Button35.Text = "UPDATE DOB";
            this.guna2Button35.Click += new System.EventHandler(this.guna2Button35_Click);
            // 
            // guna2Button36
            // 
            this.guna2Button36.AutoRoundedCorners = true;
            this.guna2Button36.BorderRadius = 14;
            this.guna2Button36.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button36.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button36.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button36.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button36.FillColor = System.Drawing.Color.Black;
            this.guna2Button36.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button36.ForeColor = System.Drawing.Color.White;
            this.guna2Button36.Location = new System.Drawing.Point(91, 339);
            this.guna2Button36.Name = "guna2Button36";
            this.guna2Button36.Size = new System.Drawing.Size(74, 31);
            this.guna2Button36.TabIndex = 17;
            this.guna2Button36.Text = "BACK";
            this.guna2Button36.Click += new System.EventHandler(this.guna2Button36_Click);
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.Black;
            this.panel63.Location = new System.Drawing.Point(670, 28);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(10, 411);
            this.panel63.TabIndex = 16;
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.Black;
            this.panel64.Controls.Add(this.panel65);
            this.panel64.Location = new System.Drawing.Point(9, 22);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(10, 411);
            this.panel64.TabIndex = 14;
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.guna2TextBox9);
            this.panel65.Controls.Add(this.guna2GradientPanel25);
            this.panel65.Controls.Add(this.guna2Button37);
            this.panel65.Controls.Add(this.guna2Button38);
            this.panel65.Controls.Add(this.panel66);
            this.panel65.Controls.Add(this.panel67);
            this.panel65.Controls.Add(this.panel68);
            this.panel65.Controls.Add(this.panel69);
            this.panel65.Controls.Add(this.guna2GradientPanel26);
            this.panel65.Location = new System.Drawing.Point(0, 249);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(690, 458);
            this.panel65.TabIndex = 30;
            // 
            // guna2TextBox9
            // 
            this.guna2TextBox9.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox9.BorderRadius = 8;
            this.guna2TextBox9.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox9.DefaultText = "";
            this.guna2TextBox9.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox9.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox9.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox9.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox9.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox9.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox9.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox9.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox9.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox9.Location = new System.Drawing.Point(203, 221);
            this.guna2TextBox9.Name = "guna2TextBox9";
            this.guna2TextBox9.PasswordChar = '\0';
            this.guna2TextBox9.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox9.PlaceholderText = "Enter New First Name";
            this.guna2TextBox9.SelectedText = "";
            this.guna2TextBox9.Size = new System.Drawing.Size(309, 38);
            this.guna2TextBox9.TabIndex = 27;
            // 
            // guna2GradientPanel25
            // 
            this.guna2GradientPanel25.BorderRadius = 8;
            this.guna2GradientPanel25.Controls.Add(this.label40);
            this.guna2GradientPanel25.Controls.Add(this.label41);
            this.guna2GradientPanel25.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel25.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel25.Location = new System.Drawing.Point(202, 158);
            this.guna2GradientPanel25.Name = "guna2GradientPanel25";
            this.guna2GradientPanel25.Size = new System.Drawing.Size(309, 38);
            this.guna2GradientPanel25.TabIndex = 13;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(221, 8);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(68, 21);
            this.label40.TabIndex = 2;
            this.label40.Text = "label40";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(7, 8);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(184, 20);
            this.label41.TabIndex = 1;
            this.label41.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button37
            // 
            this.guna2Button37.AutoRoundedCorners = true;
            this.guna2Button37.BorderRadius = 14;
            this.guna2Button37.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button37.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button37.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button37.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button37.FillColor = System.Drawing.Color.Black;
            this.guna2Button37.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button37.ForeColor = System.Drawing.Color.White;
            this.guna2Button37.Location = new System.Drawing.Point(498, 339);
            this.guna2Button37.Name = "guna2Button37";
            this.guna2Button37.Size = new System.Drawing.Size(137, 31);
            this.guna2Button37.TabIndex = 18;
            this.guna2Button37.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button38
            // 
            this.guna2Button38.AutoRoundedCorners = true;
            this.guna2Button38.BorderRadius = 14;
            this.guna2Button38.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button38.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button38.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button38.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button38.FillColor = System.Drawing.Color.Black;
            this.guna2Button38.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button38.ForeColor = System.Drawing.Color.White;
            this.guna2Button38.Location = new System.Drawing.Point(91, 339);
            this.guna2Button38.Name = "guna2Button38";
            this.guna2Button38.Size = new System.Drawing.Size(74, 31);
            this.guna2Button38.TabIndex = 17;
            this.guna2Button38.Text = "BACK";
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.Black;
            this.panel66.Location = new System.Drawing.Point(670, 28);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(10, 411);
            this.panel66.TabIndex = 16;
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.Black;
            this.panel67.Location = new System.Drawing.Point(9, 22);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(10, 411);
            this.panel67.TabIndex = 14;
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.Black;
            this.panel68.Location = new System.Drawing.Point(9, 430);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(670, 10);
            this.panel68.TabIndex = 15;
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.Color.Black;
            this.panel69.Location = new System.Drawing.Point(10, 19);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(670, 10);
            this.panel69.TabIndex = 13;
            // 
            // guna2GradientPanel26
            // 
            this.guna2GradientPanel26.BorderRadius = 20;
            this.guna2GradientPanel26.Controls.Add(this.label42);
            this.guna2GradientPanel26.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel26.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel26.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel26.Name = "guna2GradientPanel26";
            this.guna2GradientPanel26.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel26.TabIndex = 12;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Black;
            this.label42.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(7, 8);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(166, 20);
            this.label42.TabIndex = 1;
            this.label42.Text = "UPDATE FIRST NAME";
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.Black;
            this.panel70.Location = new System.Drawing.Point(9, 430);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(670, 10);
            this.panel70.TabIndex = 15;
            // 
            // panel71
            // 
            this.panel71.BackColor = System.Drawing.Color.Black;
            this.panel71.Location = new System.Drawing.Point(10, 19);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(670, 10);
            this.panel71.TabIndex = 13;
            // 
            // guna2GradientPanel27
            // 
            this.guna2GradientPanel27.BorderRadius = 20;
            this.guna2GradientPanel27.Controls.Add(this.label43);
            this.guna2GradientPanel27.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel27.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel27.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel27.Name = "guna2GradientPanel27";
            this.guna2GradientPanel27.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel27.TabIndex = 12;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Black;
            this.label43.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(44, 8);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(106, 20);
            this.label43.TabIndex = 1;
            this.label43.Text = "UPDATE DOB";
            // 
            // guna2TextBox6
            // 
            this.guna2TextBox6.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox6.BorderRadius = 8;
            this.guna2TextBox6.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox6.DefaultText = "";
            this.guna2TextBox6.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox6.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox6.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox6.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox6.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox6.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox6.Location = new System.Drawing.Point(186, 221);
            this.guna2TextBox6.Name = "guna2TextBox6";
            this.guna2TextBox6.PasswordChar = '\0';
            this.guna2TextBox6.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox6.PlaceholderText = "Enter New Email";
            this.guna2TextBox6.SelectedText = "";
            this.guna2TextBox6.Size = new System.Drawing.Size(337, 38);
            this.guna2TextBox6.TabIndex = 27;
            // 
            // guna2GradientPanel20
            // 
            this.guna2GradientPanel20.BorderRadius = 8;
            this.guna2GradientPanel20.Controls.Add(this.label32);
            this.guna2GradientPanel20.Controls.Add(this.label33);
            this.guna2GradientPanel20.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel20.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel20.Location = new System.Drawing.Point(185, 158);
            this.guna2GradientPanel20.Name = "guna2GradientPanel20";
            this.guna2GradientPanel20.Size = new System.Drawing.Size(337, 38);
            this.guna2GradientPanel20.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(143, 8);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(68, 21);
            this.label32.TabIndex = 2;
            this.label32.Text = "label32";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(7, 8);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(139, 20);
            this.label33.TabIndex = 1;
            this.label33.Text = "CURRENT EMAIL :";
            // 
            // guna2Button31
            // 
            this.guna2Button31.AutoRoundedCorners = true;
            this.guna2Button31.BorderRadius = 14;
            this.guna2Button31.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button31.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button31.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button31.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button31.FillColor = System.Drawing.Color.Black;
            this.guna2Button31.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button31.ForeColor = System.Drawing.Color.White;
            this.guna2Button31.Location = new System.Drawing.Point(498, 339);
            this.guna2Button31.Name = "guna2Button31";
            this.guna2Button31.Size = new System.Drawing.Size(137, 31);
            this.guna2Button31.TabIndex = 18;
            this.guna2Button31.Text = "UPDATE MAIL";
            this.guna2Button31.Click += new System.EventHandler(this.guna2Button31_Click);
            // 
            // guna2Button32
            // 
            this.guna2Button32.AutoRoundedCorners = true;
            this.guna2Button32.BorderRadius = 14;
            this.guna2Button32.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button32.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button32.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button32.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button32.FillColor = System.Drawing.Color.Black;
            this.guna2Button32.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button32.ForeColor = System.Drawing.Color.White;
            this.guna2Button32.Location = new System.Drawing.Point(91, 339);
            this.guna2Button32.Name = "guna2Button32";
            this.guna2Button32.Size = new System.Drawing.Size(74, 31);
            this.guna2Button32.TabIndex = 17;
            this.guna2Button32.Text = "BACK";
            this.guna2Button32.Click += new System.EventHandler(this.guna2Button32_Click);
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.Black;
            this.panel54.Location = new System.Drawing.Point(670, 28);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(10, 411);
            this.panel54.TabIndex = 16;
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.Black;
            this.panel55.Controls.Add(this.panel56);
            this.panel55.Location = new System.Drawing.Point(9, 22);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(10, 411);
            this.panel55.TabIndex = 14;
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.guna2TextBox7);
            this.panel56.Controls.Add(this.guna2GradientPanel21);
            this.panel56.Controls.Add(this.guna2Button33);
            this.panel56.Controls.Add(this.guna2Button34);
            this.panel56.Controls.Add(this.panel57);
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.panel59);
            this.panel56.Controls.Add(this.panel60);
            this.panel56.Controls.Add(this.guna2GradientPanel22);
            this.panel56.Location = new System.Drawing.Point(0, 249);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(690, 458);
            this.panel56.TabIndex = 30;
            // 
            // guna2TextBox7
            // 
            this.guna2TextBox7.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox7.BorderRadius = 8;
            this.guna2TextBox7.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox7.DefaultText = "";
            this.guna2TextBox7.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox7.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox7.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox7.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox7.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox7.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox7.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox7.Location = new System.Drawing.Point(203, 221);
            this.guna2TextBox7.Name = "guna2TextBox7";
            this.guna2TextBox7.PasswordChar = '\0';
            this.guna2TextBox7.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox7.PlaceholderText = "Enter New First Name";
            this.guna2TextBox7.SelectedText = "";
            this.guna2TextBox7.Size = new System.Drawing.Size(309, 38);
            this.guna2TextBox7.TabIndex = 27;
            // 
            // guna2GradientPanel21
            // 
            this.guna2GradientPanel21.BorderRadius = 8;
            this.guna2GradientPanel21.Controls.Add(this.label34);
            this.guna2GradientPanel21.Controls.Add(this.label35);
            this.guna2GradientPanel21.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel21.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel21.Location = new System.Drawing.Point(202, 158);
            this.guna2GradientPanel21.Name = "guna2GradientPanel21";
            this.guna2GradientPanel21.Size = new System.Drawing.Size(309, 38);
            this.guna2GradientPanel21.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(221, 8);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 21);
            this.label34.TabIndex = 2;
            this.label34.Text = "label34";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(7, 8);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(184, 20);
            this.label35.TabIndex = 1;
            this.label35.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button33
            // 
            this.guna2Button33.AutoRoundedCorners = true;
            this.guna2Button33.BorderRadius = 14;
            this.guna2Button33.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button33.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button33.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button33.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button33.FillColor = System.Drawing.Color.Black;
            this.guna2Button33.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button33.ForeColor = System.Drawing.Color.White;
            this.guna2Button33.Location = new System.Drawing.Point(498, 339);
            this.guna2Button33.Name = "guna2Button33";
            this.guna2Button33.Size = new System.Drawing.Size(137, 31);
            this.guna2Button33.TabIndex = 18;
            this.guna2Button33.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button34
            // 
            this.guna2Button34.AutoRoundedCorners = true;
            this.guna2Button34.BorderRadius = 14;
            this.guna2Button34.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button34.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button34.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button34.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button34.FillColor = System.Drawing.Color.Black;
            this.guna2Button34.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button34.ForeColor = System.Drawing.Color.White;
            this.guna2Button34.Location = new System.Drawing.Point(91, 339);
            this.guna2Button34.Name = "guna2Button34";
            this.guna2Button34.Size = new System.Drawing.Size(74, 31);
            this.guna2Button34.TabIndex = 17;
            this.guna2Button34.Text = "BACK";
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.Black;
            this.panel57.Location = new System.Drawing.Point(670, 28);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(10, 411);
            this.panel57.TabIndex = 16;
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.Black;
            this.panel58.Location = new System.Drawing.Point(9, 22);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(10, 411);
            this.panel58.TabIndex = 14;
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.Black;
            this.panel59.Location = new System.Drawing.Point(9, 430);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(670, 10);
            this.panel59.TabIndex = 15;
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.Black;
            this.panel60.Location = new System.Drawing.Point(10, 19);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(670, 10);
            this.panel60.TabIndex = 13;
            // 
            // guna2GradientPanel22
            // 
            this.guna2GradientPanel22.BorderRadius = 20;
            this.guna2GradientPanel22.Controls.Add(this.label36);
            this.guna2GradientPanel22.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel22.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel22.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel22.Name = "guna2GradientPanel22";
            this.guna2GradientPanel22.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel22.TabIndex = 12;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Black;
            this.label36.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(7, 8);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(166, 20);
            this.label36.TabIndex = 1;
            this.label36.Text = "UPDATE FIRST NAME";
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.Black;
            this.panel61.Location = new System.Drawing.Point(9, 430);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(670, 10);
            this.panel61.TabIndex = 15;
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.Black;
            this.panel62.Location = new System.Drawing.Point(10, 19);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(670, 10);
            this.panel62.TabIndex = 13;
            // 
            // guna2GradientPanel23
            // 
            this.guna2GradientPanel23.BorderRadius = 20;
            this.guna2GradientPanel23.Controls.Add(this.label37);
            this.guna2GradientPanel23.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel23.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel23.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel23.Name = "guna2GradientPanel23";
            this.guna2GradientPanel23.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel23.TabIndex = 12;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Black;
            this.label37.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(29, 8);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(121, 20);
            this.label37.TabIndex = 1;
            this.label37.Text = "UPDATE EMAIL";
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox4.BorderRadius = 8;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox4.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(186, 221);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox4.PlaceholderText = "Enter New Contact";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(337, 38);
            this.guna2TextBox4.TabIndex = 27;
            // 
            // guna2GradientPanel16
            // 
            this.guna2GradientPanel16.BorderRadius = 8;
            this.guna2GradientPanel16.Controls.Add(this.label26);
            this.guna2GradientPanel16.Controls.Add(this.label27);
            this.guna2GradientPanel16.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel16.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel16.Location = new System.Drawing.Point(185, 158);
            this.guna2GradientPanel16.Name = "guna2GradientPanel16";
            this.guna2GradientPanel16.Size = new System.Drawing.Size(337, 38);
            this.guna2GradientPanel16.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(170, 8);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 21);
            this.label26.TabIndex = 2;
            this.label26.Text = "label26";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(7, 8);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(164, 20);
            this.label27.TabIndex = 1;
            this.label27.Text = "CURRENT CONTACT :";
            // 
            // guna2Button27
            // 
            this.guna2Button27.AutoRoundedCorners = true;
            this.guna2Button27.BorderRadius = 14;
            this.guna2Button27.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button27.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button27.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button27.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button27.FillColor = System.Drawing.Color.Black;
            this.guna2Button27.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button27.ForeColor = System.Drawing.Color.White;
            this.guna2Button27.Location = new System.Drawing.Point(498, 339);
            this.guna2Button27.Name = "guna2Button27";
            this.guna2Button27.Size = new System.Drawing.Size(137, 31);
            this.guna2Button27.TabIndex = 18;
            this.guna2Button27.Text = "UPDATE CONTACT";
            this.guna2Button27.Click += new System.EventHandler(this.guna2Button27_Click);
            // 
            // guna2Button28
            // 
            this.guna2Button28.AutoRoundedCorners = true;
            this.guna2Button28.BorderRadius = 14;
            this.guna2Button28.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button28.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button28.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button28.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button28.FillColor = System.Drawing.Color.Black;
            this.guna2Button28.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button28.ForeColor = System.Drawing.Color.White;
            this.guna2Button28.Location = new System.Drawing.Point(91, 339);
            this.guna2Button28.Name = "guna2Button28";
            this.guna2Button28.Size = new System.Drawing.Size(74, 31);
            this.guna2Button28.TabIndex = 17;
            this.guna2Button28.Text = "BACK";
            this.guna2Button28.Click += new System.EventHandler(this.guna2Button28_Click);
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.Black;
            this.panel45.Location = new System.Drawing.Point(670, 28);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(10, 411);
            this.panel45.TabIndex = 16;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Black;
            this.panel46.Controls.Add(this.panel47);
            this.panel46.Location = new System.Drawing.Point(9, 22);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(10, 411);
            this.panel46.TabIndex = 14;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.guna2TextBox5);
            this.panel47.Controls.Add(this.guna2GradientPanel17);
            this.panel47.Controls.Add(this.guna2Button29);
            this.panel47.Controls.Add(this.guna2Button30);
            this.panel47.Controls.Add(this.panel48);
            this.panel47.Controls.Add(this.panel49);
            this.panel47.Controls.Add(this.panel50);
            this.panel47.Controls.Add(this.panel51);
            this.panel47.Controls.Add(this.guna2GradientPanel18);
            this.panel47.Location = new System.Drawing.Point(0, 249);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(690, 458);
            this.panel47.TabIndex = 30;
            // 
            // guna2TextBox5
            // 
            this.guna2TextBox5.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox5.BorderRadius = 8;
            this.guna2TextBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox5.DefaultText = "";
            this.guna2TextBox5.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox5.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox5.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.Location = new System.Drawing.Point(203, 221);
            this.guna2TextBox5.Name = "guna2TextBox5";
            this.guna2TextBox5.PasswordChar = '\0';
            this.guna2TextBox5.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox5.PlaceholderText = "Enter New First Name";
            this.guna2TextBox5.SelectedText = "";
            this.guna2TextBox5.Size = new System.Drawing.Size(309, 38);
            this.guna2TextBox5.TabIndex = 27;
            // 
            // guna2GradientPanel17
            // 
            this.guna2GradientPanel17.BorderRadius = 8;
            this.guna2GradientPanel17.Controls.Add(this.label28);
            this.guna2GradientPanel17.Controls.Add(this.label29);
            this.guna2GradientPanel17.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel17.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel17.Location = new System.Drawing.Point(202, 158);
            this.guna2GradientPanel17.Name = "guna2GradientPanel17";
            this.guna2GradientPanel17.Size = new System.Drawing.Size(309, 38);
            this.guna2GradientPanel17.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(221, 8);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 21);
            this.label28.TabIndex = 2;
            this.label28.Text = "label28";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(7, 8);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(184, 20);
            this.label29.TabIndex = 1;
            this.label29.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button29
            // 
            this.guna2Button29.AutoRoundedCorners = true;
            this.guna2Button29.BorderRadius = 14;
            this.guna2Button29.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button29.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button29.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button29.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button29.FillColor = System.Drawing.Color.Black;
            this.guna2Button29.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button29.ForeColor = System.Drawing.Color.White;
            this.guna2Button29.Location = new System.Drawing.Point(498, 339);
            this.guna2Button29.Name = "guna2Button29";
            this.guna2Button29.Size = new System.Drawing.Size(137, 31);
            this.guna2Button29.TabIndex = 18;
            this.guna2Button29.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button30
            // 
            this.guna2Button30.AutoRoundedCorners = true;
            this.guna2Button30.BorderRadius = 14;
            this.guna2Button30.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button30.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button30.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button30.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button30.FillColor = System.Drawing.Color.Black;
            this.guna2Button30.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button30.ForeColor = System.Drawing.Color.White;
            this.guna2Button30.Location = new System.Drawing.Point(91, 339);
            this.guna2Button30.Name = "guna2Button30";
            this.guna2Button30.Size = new System.Drawing.Size(74, 31);
            this.guna2Button30.TabIndex = 17;
            this.guna2Button30.Text = "BACK";
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.Black;
            this.panel48.Location = new System.Drawing.Point(670, 28);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(10, 411);
            this.panel48.TabIndex = 16;
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.Black;
            this.panel49.Location = new System.Drawing.Point(9, 22);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(10, 411);
            this.panel49.TabIndex = 14;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.Black;
            this.panel50.Location = new System.Drawing.Point(9, 430);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(670, 10);
            this.panel50.TabIndex = 15;
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.Black;
            this.panel51.Location = new System.Drawing.Point(10, 19);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(670, 10);
            this.panel51.TabIndex = 13;
            // 
            // guna2GradientPanel18
            // 
            this.guna2GradientPanel18.BorderRadius = 20;
            this.guna2GradientPanel18.Controls.Add(this.label30);
            this.guna2GradientPanel18.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel18.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel18.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel18.Name = "guna2GradientPanel18";
            this.guna2GradientPanel18.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel18.TabIndex = 12;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Black;
            this.label30.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(7, 8);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(166, 20);
            this.label30.TabIndex = 1;
            this.label30.Text = "UPDATE FIRST NAME";
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.Black;
            this.panel52.Location = new System.Drawing.Point(9, 430);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(670, 10);
            this.panel52.TabIndex = 15;
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.Black;
            this.panel53.Location = new System.Drawing.Point(10, 19);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(670, 10);
            this.panel53.TabIndex = 13;
            // 
            // guna2GradientPanel19
            // 
            this.guna2GradientPanel19.BorderRadius = 20;
            this.guna2GradientPanel19.Controls.Add(this.label31);
            this.guna2GradientPanel19.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel19.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel19.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel19.Name = "guna2GradientPanel19";
            this.guna2GradientPanel19.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel19.TabIndex = 12;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Black;
            this.label31.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(15, 8);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(146, 20);
            this.label31.TabIndex = 1;
            this.label31.Text = "UPDATE CONTACT";
            // 
            // guna2TextBox1
            // 
            this.guna2TextBox1.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox1.BorderRadius = 8;
            this.guna2TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox1.DefaultText = "";
            this.guna2TextBox1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox1.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.Location = new System.Drawing.Point(186, 221);
            this.guna2TextBox1.Name = "guna2TextBox1";
            this.guna2TextBox1.PasswordChar = '\0';
            this.guna2TextBox1.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox1.PlaceholderText = "Enter New Last Name";
            this.guna2TextBox1.SelectedText = "";
            this.guna2TextBox1.Size = new System.Drawing.Size(337, 38);
            this.guna2TextBox1.TabIndex = 27;
            // 
            // guna2GradientPanel12
            // 
            this.guna2GradientPanel12.BorderRadius = 8;
            this.guna2GradientPanel12.Controls.Add(this.label20);
            this.guna2GradientPanel12.Controls.Add(this.label21);
            this.guna2GradientPanel12.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel12.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel12.Location = new System.Drawing.Point(185, 158);
            this.guna2GradientPanel12.Name = "guna2GradientPanel12";
            this.guna2GradientPanel12.Size = new System.Drawing.Size(337, 38);
            this.guna2GradientPanel12.TabIndex = 13;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(183, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 21);
            this.label20.TabIndex = 2;
            this.label20.Text = "label20";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(7, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(180, 20);
            this.label21.TabIndex = 1;
            this.label21.Text = "CURRENT LAST NAME :";
            // 
            // guna2Button22
            // 
            this.guna2Button22.AutoRoundedCorners = true;
            this.guna2Button22.BorderRadius = 14;
            this.guna2Button22.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button22.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button22.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button22.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button22.FillColor = System.Drawing.Color.Black;
            this.guna2Button22.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button22.ForeColor = System.Drawing.Color.White;
            this.guna2Button22.Location = new System.Drawing.Point(498, 339);
            this.guna2Button22.Name = "guna2Button22";
            this.guna2Button22.Size = new System.Drawing.Size(137, 31);
            this.guna2Button22.TabIndex = 18;
            this.guna2Button22.Text = "UPDATE LAST NAME";
            this.guna2Button22.Click += new System.EventHandler(this.guna2Button22_Click);
            // 
            // guna2Button23
            // 
            this.guna2Button23.AutoRoundedCorners = true;
            this.guna2Button23.BorderRadius = 14;
            this.guna2Button23.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button23.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button23.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button23.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button23.FillColor = System.Drawing.Color.Black;
            this.guna2Button23.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button23.ForeColor = System.Drawing.Color.White;
            this.guna2Button23.Location = new System.Drawing.Point(91, 339);
            this.guna2Button23.Name = "guna2Button23";
            this.guna2Button23.Size = new System.Drawing.Size(74, 31);
            this.guna2Button23.TabIndex = 17;
            this.guna2Button23.Text = "BACK";
            this.guna2Button23.Click += new System.EventHandler(this.guna2Button23_Click);
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.Black;
            this.panel36.Location = new System.Drawing.Point(670, 28);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(10, 411);
            this.panel36.TabIndex = 16;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.Black;
            this.panel37.Controls.Add(this.panel38);
            this.panel37.Location = new System.Drawing.Point(9, 22);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(10, 411);
            this.panel37.TabIndex = 14;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.guna2TextBox3);
            this.panel38.Controls.Add(this.guna2GradientPanel13);
            this.panel38.Controls.Add(this.guna2Button24);
            this.panel38.Controls.Add(this.guna2Button26);
            this.panel38.Controls.Add(this.panel39);
            this.panel38.Controls.Add(this.panel40);
            this.panel38.Controls.Add(this.panel41);
            this.panel38.Controls.Add(this.panel42);
            this.panel38.Controls.Add(this.guna2GradientPanel14);
            this.panel38.Location = new System.Drawing.Point(0, 249);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(690, 458);
            this.panel38.TabIndex = 30;
            // 
            // guna2TextBox3
            // 
            this.guna2TextBox3.BorderColor = System.Drawing.Color.Black;
            this.guna2TextBox3.BorderRadius = 8;
            this.guna2TextBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox3.DefaultText = "";
            this.guna2TextBox3.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.FillColor = System.Drawing.Color.Gainsboro;
            this.guna2TextBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox3.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox3.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.Location = new System.Drawing.Point(203, 221);
            this.guna2TextBox3.Name = "guna2TextBox3";
            this.guna2TextBox3.PasswordChar = '\0';
            this.guna2TextBox3.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox3.PlaceholderText = "Enter New First Name";
            this.guna2TextBox3.SelectedText = "";
            this.guna2TextBox3.Size = new System.Drawing.Size(309, 38);
            this.guna2TextBox3.TabIndex = 27;
            // 
            // guna2GradientPanel13
            // 
            this.guna2GradientPanel13.BorderRadius = 8;
            this.guna2GradientPanel13.Controls.Add(this.label22);
            this.guna2GradientPanel13.Controls.Add(this.label23);
            this.guna2GradientPanel13.FillColor = System.Drawing.Color.Gray;
            this.guna2GradientPanel13.FillColor2 = System.Drawing.Color.DimGray;
            this.guna2GradientPanel13.Location = new System.Drawing.Point(202, 158);
            this.guna2GradientPanel13.Name = "guna2GradientPanel13";
            this.guna2GradientPanel13.Size = new System.Drawing.Size(309, 38);
            this.guna2GradientPanel13.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Segoe UI Black", 12F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(221, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 21);
            this.label22.TabIndex = 2;
            this.label22.Text = "label22";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(7, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(184, 20);
            this.label23.TabIndex = 1;
            this.label23.Text = "CURRENT FIRST NAME :";
            // 
            // guna2Button24
            // 
            this.guna2Button24.AutoRoundedCorners = true;
            this.guna2Button24.BorderRadius = 14;
            this.guna2Button24.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button24.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button24.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button24.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button24.FillColor = System.Drawing.Color.Black;
            this.guna2Button24.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button24.ForeColor = System.Drawing.Color.White;
            this.guna2Button24.Location = new System.Drawing.Point(498, 339);
            this.guna2Button24.Name = "guna2Button24";
            this.guna2Button24.Size = new System.Drawing.Size(137, 31);
            this.guna2Button24.TabIndex = 18;
            this.guna2Button24.Text = "UPDATE FIRST NAME";
            // 
            // guna2Button26
            // 
            this.guna2Button26.AutoRoundedCorners = true;
            this.guna2Button26.BorderRadius = 14;
            this.guna2Button26.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button26.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button26.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button26.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button26.FillColor = System.Drawing.Color.Black;
            this.guna2Button26.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2Button26.ForeColor = System.Drawing.Color.White;
            this.guna2Button26.Location = new System.Drawing.Point(91, 339);
            this.guna2Button26.Name = "guna2Button26";
            this.guna2Button26.Size = new System.Drawing.Size(74, 31);
            this.guna2Button26.TabIndex = 17;
            this.guna2Button26.Text = "BACK";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.Black;
            this.panel39.Location = new System.Drawing.Point(670, 28);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(10, 411);
            this.panel39.TabIndex = 16;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.Black;
            this.panel40.Location = new System.Drawing.Point(9, 22);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(10, 411);
            this.panel40.TabIndex = 14;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.Black;
            this.panel41.Location = new System.Drawing.Point(9, 430);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(670, 10);
            this.panel41.TabIndex = 15;
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.Black;
            this.panel42.Location = new System.Drawing.Point(10, 19);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(670, 10);
            this.panel42.TabIndex = 13;
            // 
            // guna2GradientPanel14
            // 
            this.guna2GradientPanel14.BorderRadius = 20;
            this.guna2GradientPanel14.Controls.Add(this.label24);
            this.guna2GradientPanel14.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel14.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel14.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel14.Name = "guna2GradientPanel14";
            this.guna2GradientPanel14.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel14.TabIndex = 12;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Black;
            this.label24.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(7, 8);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(166, 20);
            this.label24.TabIndex = 1;
            this.label24.Text = "UPDATE FIRST NAME";
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.Black;
            this.panel43.Location = new System.Drawing.Point(9, 430);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(670, 10);
            this.panel43.TabIndex = 15;
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.Black;
            this.panel44.Location = new System.Drawing.Point(10, 19);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(670, 10);
            this.panel44.TabIndex = 13;
            // 
            // guna2GradientPanel15
            // 
            this.guna2GradientPanel15.BorderRadius = 20;
            this.guna2GradientPanel15.Controls.Add(this.label25);
            this.guna2GradientPanel15.FillColor = System.Drawing.Color.Black;
            this.guna2GradientPanel15.FillColor2 = System.Drawing.Color.Black;
            this.guna2GradientPanel15.Location = new System.Drawing.Point(268, 60);
            this.guna2GradientPanel15.Name = "guna2GradientPanel15";
            this.guna2GradientPanel15.Size = new System.Drawing.Size(181, 38);
            this.guna2GradientPanel15.TabIndex = 12;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Black;
            this.label25.Font = new System.Drawing.Font("Segoe UI Black", 11F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(7, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(162, 20);
            this.label25.TabIndex = 1;
            this.label25.Text = "UPDATE LAST NAME";
            // 
            // studentmain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ulnamepanel);
            this.Controls.Add(this.panel1);
            this.Name = "studentmain";
            this.Size = new System.Drawing.Size(712, 465);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.viewpanel.ResumeLayout(false);
            this.viewpanel.PerformLayout();
            this.insertpanel.ResumeLayout(false);
            this.insertpanel.PerformLayout();
            this.deletepanel.ResumeLayout(false);
            this.deletepanel.PerformLayout();
            this.updatepanel.ResumeLayout(false);
            this.updatepanel.PerformLayout();
            this.updatepaneloptions.ResumeLayout(false);
            this.updatepaneloptions.PerformLayout();
            this.unamepanel.ResumeLayout(false);
            this.guna2GradientPanel9.ResumeLayout(false);
            this.guna2GradientPanel9.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.guna2GradientPanel10.ResumeLayout(false);
            this.guna2GradientPanel10.PerformLayout();
            this.guna2GradientPanel11.ResumeLayout(false);
            this.guna2GradientPanel11.PerformLayout();
            this.guna2GradientPanel8.ResumeLayout(false);
            this.guna2GradientPanel8.PerformLayout();
            this.guna2GradientPanel7.ResumeLayout(false);
            this.guna2GradientPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2DataGridView3)).EndInit();
            this.guna2GradientPanel6.ResumeLayout(false);
            this.guna2GradientPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2DataGridView2)).EndInit();
            this.guna2GradientPanel5.ResumeLayout(false);
            this.guna2GradientPanel5.PerformLayout();
            this.guna2GradientPanel3.ResumeLayout(false);
            this.guna2GradientPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2DataGridView1)).EndInit();
            this.guna2GradientPanel4.ResumeLayout(false);
            this.guna2GradientPanel4.PerformLayout();
            this.guna2GradientPanel2.ResumeLayout(false);
            this.guna2GradientPanel2.PerformLayout();
            this.guna2GradientPanel1.ResumeLayout(false);
            this.guna2GradientPanel1.PerformLayout();
            this.ulnamepanel.ResumeLayout(false);
            this.UCONTPANEL.ResumeLayout(false);
            this.umailpanel.ResumeLayout(false);
            this.UPDATEMPANEL.ResumeLayout(false);
            this.genderUpanel.ResumeLayout(false);
            this.guna2GradientPanel28.ResumeLayout(false);
            this.guna2GradientPanel28.PerformLayout();
            this.panel73.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.guna2GradientPanel29.ResumeLayout(false);
            this.guna2GradientPanel29.PerformLayout();
            this.guna2GradientPanel30.ResumeLayout(false);
            this.guna2GradientPanel30.PerformLayout();
            this.guna2GradientPanel31.ResumeLayout(false);
            this.guna2GradientPanel31.PerformLayout();
            this.guna2GradientPanel24.ResumeLayout(false);
            this.guna2GradientPanel24.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.guna2GradientPanel25.ResumeLayout(false);
            this.guna2GradientPanel25.PerformLayout();
            this.guna2GradientPanel26.ResumeLayout(false);
            this.guna2GradientPanel26.PerformLayout();
            this.guna2GradientPanel27.ResumeLayout(false);
            this.guna2GradientPanel27.PerformLayout();
            this.guna2GradientPanel20.ResumeLayout(false);
            this.guna2GradientPanel20.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel56.ResumeLayout(false);
            this.guna2GradientPanel21.ResumeLayout(false);
            this.guna2GradientPanel21.PerformLayout();
            this.guna2GradientPanel22.ResumeLayout(false);
            this.guna2GradientPanel22.PerformLayout();
            this.guna2GradientPanel23.ResumeLayout(false);
            this.guna2GradientPanel23.PerformLayout();
            this.guna2GradientPanel16.ResumeLayout(false);
            this.guna2GradientPanel16.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.guna2GradientPanel17.ResumeLayout(false);
            this.guna2GradientPanel17.PerformLayout();
            this.guna2GradientPanel18.ResumeLayout(false);
            this.guna2GradientPanel18.PerformLayout();
            this.guna2GradientPanel19.ResumeLayout(false);
            this.guna2GradientPanel19.PerformLayout();
            this.guna2GradientPanel12.ResumeLayout(false);
            this.guna2GradientPanel12.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.guna2GradientPanel13.ResumeLayout(false);
            this.guna2GradientPanel13.PerformLayout();
            this.guna2GradientPanel14.ResumeLayout(false);
            this.guna2GradientPanel14.PerformLayout();
            this.guna2GradientPanel15.ResumeLayout(false);
            this.guna2GradientPanel15.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel2;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private System.Windows.Forms.Panel viewpanel;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2DataGridView guna2DataGridView1;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private Guna.UI2.WinForms.Guna2Button guna2Button6;
        private System.Windows.Forms.Panel deletepanel;
        private System.Windows.Forms.Panel updatepanel;
        private Guna.UI2.WinForms.Guna2Button guna2Button9;
        private Guna.UI2.WinForms.Guna2Button guna2Button10;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Guna.UI2.WinForms.Guna2Button guna2Button8;
        private Guna.UI2.WinForms.Guna2Button guna2Button7;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2Button guna2Button11;
        private Guna.UI2.WinForms.Guna2TextBox fnamebox;
        private Guna.UI2.WinForms.Guna2TextBox Lnamebox;
        private Guna.UI2.WinForms.Guna2TextBox mailbo;
        private Guna.UI2.WinForms.Guna2TextBox contatcbox;
        private System.Windows.Forms.Panel insertpanel;
        private Guna.UI2.WinForms.Guna2ComboBox genderbox;
        private Guna.UI2.WinForms.Guna2DateTimePicker dobbox;
        private Guna.UI2.WinForms.Guna2DataGridView guna2DataGridView3;
        private Guna.UI2.WinForms.Guna2DataGridView guna2DataGridView2;
        private System.Windows.Forms.Panel updatepaneloptions;
        private Guna.UI2.WinForms.Guna2Button guna2Button14;
        private Guna.UI2.WinForms.Guna2Button guna2Button13;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private Guna.UI2.WinForms.Guna2Button guna2Button19;
        private Guna.UI2.WinForms.Guna2Button guna2Button18;
        private Guna.UI2.WinForms.Guna2Button guna2Button17;
        private Guna.UI2.WinForms.Guna2Button guna2Button16;
        private Guna.UI2.WinForms.Guna2Button guna2Button15;
        private System.Windows.Forms.Panel unamepanel;
        private Guna.UI2.WinForms.Guna2Button guna2Button25;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel8;
        private System.Windows.Forms.Label label14;
        private Guna.UI2.WinForms.Guna2Button guna2Button12;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private Guna.UI2.WinForms.Guna2TextBox updatefnamebox;
        private System.Windows.Forms.Panel panel6;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox2;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private Guna.UI2.WinForms.Guna2Button guna2Button20;
        private Guna.UI2.WinForms.Guna2Button guna2Button21;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel34;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel ulnamepanel;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox1;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private Guna.UI2.WinForms.Guna2Button guna2Button22;
        private Guna.UI2.WinForms.Guna2Button guna2Button23;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel38;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox3;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel13;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private Guna.UI2.WinForms.Guna2Button guna2Button24;
        private Guna.UI2.WinForms.Guna2Button guna2Button26;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel42;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel14;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel44;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel UCONTPANEL;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel16;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private Guna.UI2.WinForms.Guna2Button guna2Button27;
        private Guna.UI2.WinForms.Guna2Button guna2Button28;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel47;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox5;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel17;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Guna.UI2.WinForms.Guna2Button guna2Button29;
        private Guna.UI2.WinForms.Guna2Button guna2Button30;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel51;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel18;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel53;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel19;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel umailpanel;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox6;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel20;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private Guna.UI2.WinForms.Guna2Button guna2Button31;
        private Guna.UI2.WinForms.Guna2Button guna2Button32;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Panel panel56;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox7;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel21;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private Guna.UI2.WinForms.Guna2Button guna2Button33;
        private Guna.UI2.WinForms.Guna2Button guna2Button34;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel60;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel22;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Panel panel62;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel23;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel UPDATEMPANEL;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel24;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private Guna.UI2.WinForms.Guna2Button guna2Button35;
        private Guna.UI2.WinForms.Guna2Button guna2Button36;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel65;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox9;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel25;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private Guna.UI2.WinForms.Guna2Button guna2Button37;
        private Guna.UI2.WinForms.Guna2Button guna2Button38;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel69;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel26;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Panel panel71;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel27;
        private System.Windows.Forms.Label label43;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker2;
        private System.Windows.Forms.Panel genderUpanel;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel28;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private Guna.UI2.WinForms.Guna2Button guna2Button39;
        private Guna.UI2.WinForms.Guna2Button guna2Button40;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Panel panel74;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox8;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel29;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private Guna.UI2.WinForms.Guna2Button guna2Button41;
        private Guna.UI2.WinForms.Guna2Button guna2Button42;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Panel panel78;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel30;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Panel panel80;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel31;
        private System.Windows.Forms.Label label49;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox2;
    }
}
