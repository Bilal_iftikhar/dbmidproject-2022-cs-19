﻿using Guna.UI2.WinForms;
using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class assignadvisor : UserControl
    {
        private int projectId;
        public string title = " ";
        public string mainadv = " ";
        public string coadv = " ";
        public string indusadv = " ";

        public assignadvisor()
        {  
            InitializeComponent();
            panel1.Visible = false;
            panel15.Visible = false;
            ProjectToComboBox();
            ProjectToComboBox2();
            advisorsBOX();
            comBOX();
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //view
            assignADVISORcrudpanel.Visible = true;
            panel1.Visible = true;
            panel10.Visible = true;
            panel15.Visible = false;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Project.Title, MAX(CASE WHEN Lookup.Value = 'Main Advisor' THEN Person.Email END) AS [Main Advisor], MAX(CASE WHEN Lookup.Value = 'Co-Advisror' THEN Person.Email END) AS [Co-Advisor], MAX(CASE WHEN Lookup.Value = 'Industry Advisor' THEN Person.Email END) AS [Industry Advisor], MAX(ProjectAdvisor.AssignmentDate) AS AssignmentDate FROM Project JOIN ProjectAdvisor ON Project.Id = ProjectAdvisor.ProjectId JOIN Advisor ON Advisor.Id = ProjectAdvisor.AdvisorId JOIN Person ON Advisor.Id = Person.Id JOIN Lookup ON ProjectAdvisor.AdvisorRole = Lookup.Id WHERE Lookup.Value IN ('Main Advisor', 'Co-Advisror', 'Industry Advisor') GROUP BY Project.Title, ProjectAdvisor.ProjectId;\r\n", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //assignadvisor
            assignADVISORcrudpanel.Visible  = true;
            panel1.Visible = true;
            panel10.Visible = false;
            panel15.Visible = false;
        }
        private void guna2Button4_Click_1(object sender, EventArgs e)
        {
            //BACKFROMASSIGN
            assignADVISORcrudpanel.Visible = true;
            panel1.Visible = false;
            panel15.Visible = false;
        }
        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //ASSIGN
            try
            {
                bool check = true;
                var con = Configuration.getInstance().getConnection();
                if (MainAdvComboBox.Text != null && MainAdvComboBox.Text != "")
                {
                    bool chk = assignRole(con, "Main Advisor", MainAdvComboBox.Text);
                    if (chk == false)
                    {
                        check = false;
                    }
                }
                if (CoAdvComboBox.Text != null && CoAdvComboBox.Text != "" )
                {
                    bool chk = assignRole(con, "Co-Advisror", CoAdvComboBox.Text);
                    if (chk == false)
                    {
                        check = false;
                    }
                }
                if (IndustryAdvComboBox.Text != null && IndustryAdvComboBox.Text != "")
                {
                    bool chk = assignRole(con, "Industry Advisor", IndustryAdvComboBox.Text);
                    if (chk == false)
                    {
                        check = false;
                    }
                }
                if (check == true)
                {
                    MessageBox.Show("Successfully saved");
                }
                else
                {
                    MessageBox.Show("An Error Occured While Adding Data, (Can not Assign Same Advisors)");  
                }
            }
            catch
            {
                MessageBox.Show("An Error Occured While Running the Query");
            }
        }
        private void PROJECTComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {   
        }
        private void TitleComboBox_DropDown(object sender, EventArgs e)
        {
        }
        private void MainAdvComboBox_DropDown(object sender, EventArgs e)
        {
          
        }
        private void CoAdvComboBox_DropDown(object sender, EventArgs e)
        {
        }
        private void IndustryAdvComboBox_DropDown(object sender, EventArgs e)
        { 
        }
        private void ProjectToComboBox()
        {
            TitleComboBox.Items.Clear();
            TitleComboBox.Items.Add("");
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand cmd = new SqlCommand("SELECT Title FROM Project", con))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string firstName = reader["Title"].ToString();
                            TitleComboBox.Items.Add(firstName);
                        }
                    }
                }
            }
        }
        private void ProjectToComboBox2()
        {
            guna2ComboBox4.Items.Clear();
            guna2ComboBox4.Items.Add("");
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand cmd = new SqlCommand("SELECT Title FROM Project", con))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string firstName = reader["Title"].ToString();
                            guna2ComboBox4.Items.Add(firstName);
                        }
                    }
                }
            }
        }
        public void advisorsBOX()
        {
            MainAdvComboBox.Items.Clear();
            CoAdvComboBox.Items.Clear();
            IndustryAdvComboBox.Items.Clear();

            MainAdvComboBox.Items.Add("");
            CoAdvComboBox.Items.Add("");
            IndustryAdvComboBox.Items.Add("");
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand cmd = new SqlCommand("SELECT Person.Email AS Email FROM Person Join Advisor ON Person.Id = Advisor.Id", con))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string firstName = reader["Email"].ToString();
                            MainAdvComboBox.Items.Add(firstName);
                            CoAdvComboBox.Items.Add(firstName);
                            IndustryAdvComboBox.Items.Add(firstName);
                        }
                    }
                }
            }
        }
        public void comBOX()
        {
            guna2ComboBox3.Items.Clear();
            guna2ComboBox2.Items.Clear();
            guna2ComboBox1.Items.Clear();

            guna2ComboBox3.Items.Add("");
            guna2ComboBox2.Items.Add("");
            guna2ComboBox1.Items.Add("");
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand cmd = new SqlCommand("SELECT Person.Email AS Email FROM Person Join Advisor ON Person.Id = Advisor.Id", con))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string firstName = reader["Email"].ToString();
                            guna2ComboBox3.Items.Add(firstName);
                            guna2ComboBox2.Items.Add(firstName);
                            guna2ComboBox1.Items.Add(firstName);
                        }
                    }
                }
            }
        }
        bool assignRole(SqlConnection con, string role, string advId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Insert into ProjectAdvisor values ((select Id from Person Where Email = @AdvisorId), (select Id from Project Where Title = @ProjectId), (select Id from Lookup Where value = @AdvisorRole), @AssignmentDate); ", con);
                cmd.Parameters.AddWithValue("@ProjectId", TitleComboBox.Text);
                cmd.Parameters.AddWithValue("@AdvisorId", advId);
                cmd.Parameters.AddWithValue("@AdvisorRole", role);
                cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //backfromview
            assignADVISORcrudpanel.Visible = true;
            panel1.Visible = false;
            panel10.Visible = false;
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            //UPDATE
            assignADVISORcrudpanel.Visible = true;
            panel1.Visible = true;
            panel10.Visible = true;
            panel15.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Project.Title, MAX(CASE WHEN Lookup.Value = 'Main Advisor' THEN Person.Email END) AS [Main Advisor], MAX(CASE WHEN Lookup.Value = 'Co-Advisror' THEN Person.Email END) AS [Co-Advisor], MAX(CASE WHEN Lookup.Value = 'Industry Advisor' THEN Person.Email END) AS [Industry Advisor], MAX(ProjectAdvisor.AssignmentDate) AS AssignmentDate FROM Project JOIN ProjectAdvisor ON Project.Id = ProjectAdvisor.ProjectId JOIN Advisor ON Advisor.Id = ProjectAdvisor.AdvisorId JOIN Person ON Advisor.Id = Person.Id JOIN Lookup ON ProjectAdvisor.AdvisorRole = Lookup.Id WHERE Lookup.Value IN ('Main Advisor', 'Co-Advisror', 'Industry Advisor') GROUP BY Project.Title, ProjectAdvisor.ProjectId;\r\n", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView3.DataSource = dt;
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //UPDATEBTN
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorId = (select Id from Person Where Email = @mainAdvisorId) WHERE AdvisorId = (select Id from Person Where Email = @mainAdvisorId2) AND ProjectId = (select Id from Project Where Title = @ProjectId) AND AdvisorRole = (select Id from Lookup Where value = @mainAdvisorRole); UPDATE ProjectAdvisor SET AdvisorId = (select Id from Person Where Email = @coAdvisorId) WHERE AdvisorId = (select Id from Person Where Email = @coAdvisorId2) AND ProjectId = (select Id from Project Where Title = @ProjectId) AND AdvisorRole = (select Id from Lookup Where value = @coAdvisorRole); UPDATE ProjectAdvisor SET AdvisorId = (select Id from Person Where Email = @indAdvisorId) WHERE AdvisorId = (select Id from Person Where Email = @indAdvisorId2) AND ProjectId = (select Id from Project Where Title = @ProjectId) AND AdvisorRole = (select Id from Lookup Where value = @indAdvisorRole); ", con);
            cmd.Parameters.AddWithValue("@ProjectId", guna2ComboBox4.Text);
            cmd.Parameters.AddWithValue("@mainAdvisorId", (guna2ComboBox3.Text));
            cmd.Parameters.AddWithValue("@mainAdvisorId2", (mainadv));
            cmd.Parameters.AddWithValue("@coAdvisorId", guna2ComboBox2.Text);
            cmd.Parameters.AddWithValue("@coAdvisorId2", coadv);
            cmd.Parameters.AddWithValue("@indAdvisorId", guna2ComboBox1.Text);
            cmd.Parameters.AddWithValue("@indAdvisorId2", indusadv);
            cmd.Parameters.AddWithValue("@mainAdvisorRole", "Main Advisor");
            cmd.Parameters.AddWithValue("@coAdvisorRole", "Co-Advisror");
            cmd.Parameters.AddWithValue("@indAdvisorRole", "Industry Advisor");
            cmd.ExecuteNonQuery();

            bool check = true;
            if (guna2ComboBox3 == null || guna2ComboBox3.Text != "" )
            {
                bool chk = assignRole(con, "Main Advisor", guna2ComboBox3.Text);
                if (chk == false)
                {
                    check = false;
                }
            }
            if (guna2ComboBox2 == null && guna2ComboBox2.Text != "")
            {
                bool chk = assignRole(con, "Co-Advisror", guna2ComboBox2.Text);
                if (chk == false)
                {
                    check = false;
                }
            }
            if (guna2ComboBox1 == null && guna2ComboBox1.Text != "")
            {
                bool chk = assignRole(con, "Industry Advisor", guna2ComboBox1.Text);
                if (chk == false)
                {
                    check = false;
                }
            }
            if (check == true)
            {
                MessageBox.Show("Successfully Updated");
               
            }
            else
            {
                MessageBox.Show("Updated");

            }
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            assignADVISORcrudpanel.Visible = true;
            panel1.Visible = false;
            panel10.Visible = false;
        }

        private void guna2DataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                guna2DataGridView3.ClearSelection();

                guna2DataGridView3.Rows[e.RowIndex].Selected = true;

                DataGridViewRow selectedRow = guna2DataGridView3.Rows[e.RowIndex];

                title = selectedRow.Cells["Title"].Value.ToString();
                mainadv = selectedRow.Cells["Main Advisor"].Value.ToString();
                coadv = selectedRow.Cells["Co-Advisor"].Value.ToString();
                indusadv = selectedRow.Cells["Industry Advisor"].Value.ToString();

                guna2ComboBox4.Text = title;
                guna2ComboBox3.Text = mainadv;
                guna2ComboBox2.Text = coadv;
                guna2ComboBox1.Text = indusadv;

            }
        }
    }
}