﻿using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class studentmain : UserControl
    {
        public string selectedfNameU;
        public string selectedlNameU;
        public string selectedconU;
        public string selectedmailU;
        public string selecteddobU;
        public int selectedgenU;
        public string selectedregU;


        public studentmain()
        {
            InitializeComponent();
            guna2DataGridView1.ColumnHeadersHeight = 35;
            guna2DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView2.ColumnHeadersHeight = 35;
            guna2DataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            guna2DataGridView3.ColumnHeadersHeight = 35;
            guna2DataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            guna2DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            viewpanel.Visible = false;  
            insertpanel.Visible = false;    
            deletepanel.Visible = false;    
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
            unamepanel.Visible = false;
            ulnamepanel.Visible = false;
            umailpanel.Visible = false;
            UPDATEMPANEL.Visible = false;
            genderUpanel.Visible = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            //viewstudentbtn
          
            viewpanel.Visible = true;
            guna2DataGridView1.MultiSelect = false;
            guna2DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.RegistrationNo,p.FirstName,p.LastName,p.Contact,p.Email,p.DateOfBirth,p.Gender From Person p Join Student s on p.Id = s.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }
        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //backfromview
            guna2Button5.FillColor = Color.DimGray;
            viewpanel.Visible=false;
            panel1.Visible=true;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //insertstudentbtn
            panel1.Visible = true;
            viewpanel.Visible = true;
            insertpanel.Visible=true;
        }
        private void guna2Button6_Click(object sender, EventArgs e)
        {
            //backfrominsert
            panel1.Visible = true;
            viewpanel.Visible = false;
            insertpanel.Visible = false;
        }
        private void guna2Button11_Click(object sender, EventArgs e)
        {
            // addstudentbtn
          
        }
        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //deletestudentbtn
            panel1.Visible = true;
            viewpanel.Visible = true;
            insertpanel.Visible = true;
            deletepanel.Visible = true;
            guna2DataGridView2.MultiSelect = false;
            guna2DataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.FirstName,p.LastName,p.Contact,p.Email,p.DateOfBirth,p.Gender,s.RegistrationNo From Person p Join Student s on p.Id = s.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView2.DataSource = dt;
        }
        private void guna2Button7_Click(object sender, EventArgs e)
        {
            //backfromDELETE
            panel1.Visible = true;
            viewpanel.Visible = false;
            insertpanel.Visible = false;
            deletepanel.Visible = false;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //updatestudentbtn
            panel1.Visible = true;
            viewpanel.Visible = true;
            insertpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            guna2DataGridView3.MultiSelect = false;
            guna2DataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select p.FirstName,p.LastName,p.Contact,p.Email,p.DateOfBirth,p.Gender,s.RegistrationNo From Person p Join Student s on p.Id = s.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView3.DataSource = dt;
        }
        private void guna2Button10_Click(object sender, EventArgs e)
        {
            //backfromupdate
            panel1.Visible = true;
            viewpanel.Visible = false;
            insertpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            //DELETEPANELBTN
        }


        private void guna2Button9_Click(object sender, EventArgs e)
        {
            //updatePANELBTN
            if (guna2DataGridView3.SelectedRows.Count > 0)
            {
                selectedfNameU = guna2DataGridView3.SelectedRows[0].Cells["FirstName"].Value.ToString();
                selectedlNameU = guna2DataGridView3.SelectedRows[0].Cells["LastName"].Value.ToString();
                selectedconU = guna2DataGridView3.SelectedRows[0].Cells["Contact"].Value.ToString();
                selectedmailU = guna2DataGridView3.SelectedRows[0].Cells["Email"].Value.ToString();
                selecteddobU = guna2DataGridView3.SelectedRows[0].Cells["DateOfBirth"].Value.ToString();
                string gendU = guna2DataGridView3.SelectedRows[0].Cells["Gender"].Value.ToString();
                selectedregU = guna2DataGridView3.SelectedRows[0].Cells["RegistrationNo"].Value.ToString();
                panel1.Visible = true;
                insertpanel.Visible = true;
                viewpanel.Visible = true;
                deletepanel.Visible = true;
                updatepanel.Visible = true;
                updatepaneloptions.Visible=true;
            }
            else
            {
                MessageBox.Show("Select any row to update the record");
            }

        }

        private void guna2Button14_Click(object sender, EventArgs e)
        {
            //UPDATEFIRSTNAME
            panel1.Visible = true;
            insertpanel.Visible = true;
            viewpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            updatepaneloptions.Visible = true;
            unamepanel.Visible  = true;
            label16.Text = selectedfNameU;

        }
        private void guna2Button15_Click(object sender, EventArgs e)
        {
            //UPDATELASTNAME
            panel1.Visible = true;
            insertpanel.Visible = true;
            viewpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            updatepaneloptions.Visible = true;
            unamepanel.Visible = true;
            ulnamepanel.Visible = true;
            label20.Text = selectedlNameU;
        }
        private void guna2Button16_Click(object sender, EventArgs e)
        {
            //updatecontatc
            panel1.Visible = true;
            insertpanel.Visible = true;
            viewpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            updatepaneloptions.Visible = true;
            unamepanel.Visible = true;
            ulnamepanel.Visible = true;
            updatepanel.Visible = true;        
            label26.Text = selectedconU;
        }

        private void guna2Button17_Click(object sender, EventArgs e)
        {
            //UPDATEMAIL
            panel1.Visible = true;
            insertpanel.Visible = true;
            viewpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            updatepaneloptions.Visible = true;
            unamepanel.Visible = true;
            ulnamepanel.Visible = true;
            updatepanel.Visible = true;
            umailpanel.Visible = true;  
            label32.Text = selectedmailU;
            guna2TextBox6.Text= selectedmailU;

        }

        private void guna2Button19_Click(object sender, EventArgs e)
        {
            //UPDATEDOB
            panel1.Visible = true;
            insertpanel.Visible = true;
            viewpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            updatepaneloptions.Visible = true;
            unamepanel.Visible = true;
            ulnamepanel.Visible = true;
            updatepanel.Visible = true;
            umailpanel.Visible = true;
            UPDATEMPANEL.Visible = true;
            label38.Text = selecteddobU;
        }

        private void guna2Button18_Click(object sender, EventArgs e)
        {
            //UPDATEGENDER
            panel1.Visible = true;
            insertpanel.Visible = true;
            viewpanel.Visible = true;
            deletepanel.Visible = true;
            updatepanel.Visible = true;
            updatepaneloptions.Visible = true;
            unamepanel.Visible = true;
            ulnamepanel.Visible = true;
            updatepanel.Visible = true;
            umailpanel.Visible = true;
            UPDATEMPANEL.Visible = true;
            genderUpanel.Visible = true;
         //   label44.Text = selectedgenU;

        }
        private void updatepaneloptions_Paint(object sender, PaintEventArgs e)
        {

        }
        private void viewpanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void insertpanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void deletepanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button13_Click(object sender, EventArgs e)
        {
            //BACKFROMUPDATEOPTIONS
            panel1.Visible = true;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
        }

        private void guna2Button25_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
            unamepanel.Visible = false;
        }
        private void guna2Button23_Click(object sender, EventArgs e)
        {
            //backfromulpanel
            panel1.Visible = false;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = true;
            unamepanel.Visible = false;
            ulnamepanel.Visible = false;
        }
        private void guna2Button28_Click(object sender, EventArgs e)
        {
            //BACKUCPANEL
            panel1.Visible = true;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
            unamepanel.Visible = false;
            ulnamepanel.Visible = false;
            updatepanel.Visible = false;
        }
        private void guna2Button32_Click(object sender, EventArgs e)
        {
            //BACKUCPANEL
            panel1.Visible = true;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
            unamepanel.Visible = false;
            ulnamepanel.Visible = false;
            updatepanel.Visible = false;
            umailpanel.Visible = false;
        }
        private void guna2Button36_Click(object sender, EventArgs e)
        {
            //BACKUCPANEL
            panel1.Visible = true;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
            unamepanel.Visible = false;
            ulnamepanel.Visible = false;
            updatepanel.Visible = false;
            umailpanel.Visible = false;
            UPDATEMPANEL.Visible = false;
        }
        private void guna2Button40_Click(object sender, EventArgs e)
        {
            //BACKGENDERPANEL
            panel1.Visible = true;
            insertpanel.Visible = false;
            viewpanel.Visible = false;
            deletepanel.Visible = false;
            updatepanel.Visible = false;
            updatepaneloptions.Visible = false;
            unamepanel.Visible = false;
            ulnamepanel.Visible = false;
            updatepanel.Visible = false;
            umailpanel.Visible = false;
            UPDATEMPANEL.Visible = false;
            genderUpanel.Visible = false;   
        }
        private void guna2Button12_Click(object sender, EventArgs e)
        {
            //updatefirstnamecode
        }

        private void guna2Button22_Click(object sender, EventArgs e)
        {
            //updatelastnamecode
        }

        
        private void guna2Button27_Click(object sender, EventArgs e)
        {
            //updateCONTACTcode
        }

        private void guna2Button31_Click(object sender, EventArgs e)
        {
            //updatmailcode
        }

        private void guna2Button35_Click(object sender, EventArgs e)
        {
            //updatdobcode
        }

        private void guna2Button39_Click(object sender, EventArgs e)
        {
            //updategender
        }
        public void returngender()
        {
            if (genderbox.Text == "Male")
            {
                selectedgenU = 1;
            }
            else if (genderbox.Text == "Femaale")
            {
                selectedgenU = 2; 
            }
        }
        public int amxid()
        {
            string selectMaxIdQuery = "SELECT Top 1 Id FROM Person ORDER BY Id DESC ;";
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand(selectMaxIdQuery, con);
            return (int)command.ExecuteScalar();
        }

    }
}
