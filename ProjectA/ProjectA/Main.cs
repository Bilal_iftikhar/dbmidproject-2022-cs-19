﻿using Guna.UI2.WinForms;
using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            home1.Visible = true;
            studentmain1.Visible = false;
            student1.Visible    = false;
            advisor1.Visible = false;
            projects1.Visible = false;
            evaluation1.Visible = false;
            markevaluation1.Visible = false;
            
           
        }
       
        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void sidebar1_Paint(object sender, PaintEventArgs e)
        {

        }


        private void guna2Button2_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            studentmain1.Visible = false;
            student1.Visible = true;
            advisor1.Visible = false;
            projects1.Visible = false;
            markevaluation1.Visible = false;
        }

        private void guna2Button1_CheckedChanged(object sender, EventArgs e)
        {
            moveImageBox(sender);
            if (guna2Button1.Checked == false)
            {
                guna2Button1.Image = Properties.Resources.equal_housing_opportunity_30px;
            }
            else if (guna2Button1.Checked == true)
            {
                guna2Button1.Image = Properties.Resources.home_50px;
            }
        }

        private void guna2Button2_CheckedChanged(object sender, EventArgs e)
        {

            if (guna2Button2.Checked == false)
            {
                guna2Button2.Image = Properties.Resources.Student_Male_64pxw;
            }
            else if (guna2Button2.Checked == true)
            {
                guna2Button2.Image = Properties.Resources.student_registration_50px;
            }
        }

        private void guna2Button3_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button3.Checked == false)
            {
                guna2Button3.Image = Properties.Resources.consultation_50pxw;
            }
            else if (guna2Button3.Checked == true)
            {
                guna2Button3.Image = Properties.Resources.consultation_24px;
            }
        }

        private void guna2Button4_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button4.Checked == false)
            {
                guna2Button4.Image = Properties.Resources.project_24pxw;
            }
            else if (guna2Button4.Checked == true)
            {
                guna2Button4.Image = Properties.Resources.group_of_projects_24px;
            }
        }

        private void guna2Button5_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button5.Checked == false)
            {
                guna2Button5.Image = Properties.Resources.scorecard_24pxw1;
            }
            else if (guna2Button5.Checked == true)
            {
                guna2Button5.Image = Properties.Resources.report_card_26px;
            }
        }

        private void guna2Button6_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button6.Checked == false)
            {
                guna2Button6.Image = Properties.Resources.test_50pxw2;
            }
            else if (guna2Button6.Checked == true)
            {
                guna2Button6.Image = Properties.Resources.report_card_26px;
            }
        }

        private void guna2Button7_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button7.Checked == false)
            {
                guna2Button7.Image = Properties.Resources.user_groups_24pxw;
            }
            else if (guna2Button7.Checked == true)
            {
                guna2Button7.Image = Properties.Resources.user_groups_32px;
            }
        }

        private void guna2Button8_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button8.Checked == false)
            {
                guna2Button8.Image = Properties.Resources.consultation_50pxw;
            }
            else if (guna2Button8.Checked == true)
            {
                guna2Button8.Image = Properties.Resources.consultation_24px;
            }
        }

        private void guna2Button9_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2Button9.Checked == false)
            {
                guna2Button9.Image = Properties.Resources.business_report_24pxw;
            }
            else if (guna2Button9.Checked == true)
            {
                guna2Button9.Image = Properties.Resources.business_report_24px;
            }
        }
        private void moveImageBox(object sender)
        {
            Guna2Button b = (Guna2Button)sender;
            img1.Location = new Point(b.Location.X + 184, b.Location.Y - 19);
            img1.SendToBack();

        }
        private void guna2Button1_Click(object sender, EventArgs e)
        {
            home1.Visible = true;
            studentmain1.Visible = false;
            student1.Visible = false;
            projects1.Visible = false;
            advisor1.Visible = false;
            evaluation1.Visible = false;
            markevaluation1.Visible = false;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            home1.Visible=false;
            studentmain1.Visible=false;
            student1.Visible = false;
            advisor1.Visible=true;
            projects1.Visible = false;
            evaluation1.Visible = false;
            markevaluation1.Visible = false;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            studentmain1.Visible = false;
            student1.Visible = false;
            advisor1.Visible = false;
            projects1.Visible = true;
            evaluation1.Visible = false;
            markevaluation1.Visible = false;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            studentmain1.Visible = false;
            advisor1.Visible = false;
            student1.Visible = false;
            projects1.Visible = false;
            evaluation1.Visible = true;
            markevaluation1.Visible = false;
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            studentmain1.Visible = false;
            advisor1.Visible = false;
            student1.Visible = false;
            projects1.Visible = false;
            evaluation1.Visible = false;
            markevaluation1.Visible = true;
        }

        private void studentmain2_Load(object sender, EventArgs e)
        {

        }
    }
}
