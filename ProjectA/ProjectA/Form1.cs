﻿using MidprojectA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        void LoadCSV()
        {
            string filePath = "midsData.csv";

            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not found: " + filePath);
                return;
            }

            // Read the CSV file
            using (StreamReader reader = new StreamReader(filePath))
            {
                int a = 0;
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    string[] fields = line.Split(',') ;

                    if (a > 0)
                    {
                        DateTime date;
                        if (DateTime.TryParse(fields[7].Trim(), out date))
                        {
                            int gender;
                            if (fields[8].Trim() == "Male")
                            {
                                gender = 1;
                            }
                            else if (fields[8].Trim() == "Female")
                            {
                                gender = 2;
                            }
                            else
                            {
                                gender = 0;
                            }

                            LoadDataToDBStudent(a, fields[2]);
                            //LoadDataToDBPerson(fields[3].Trim(), fields[4].Trim(), fields[5].Trim(), fields[6].Trim(), date, gender);
                        }
                        else
                        {
                            // Handle the case where parsing fails, such as displaying an error message.
                            Console.WriteLine("Error parsing date at line " + a);
                        }
                    }



                    a++;

                    Console.WriteLine();
                }
            }
            MessageBox.Show("Successfully saved");
        }
        void LoadDataToDBStudent(int id, string regNo)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student values (@Id, @RegistrationNo);", con);
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.Parameters.AddWithValue("@RegistrationNo", regNo);
            cmd.ExecuteNonQuery();
        }

        void LoadDataToDBPerson(string fName, string lName, string contact, string email, DateTime dateTime, int gender)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO Person VALUES ( @FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender)", con);
            cmd.Parameters.AddWithValue("@FirstName", fName);
            cmd.Parameters.AddWithValue("@LastName", lName);
            cmd.Parameters.AddWithValue("@Contact", contact);
            cmd.Parameters.AddWithValue("@Email", email);
            cmd.Parameters.AddWithValue("@DateOfBirth", dateTime);
            cmd.Parameters.AddWithValue("@Gender", gender);
            cmd.ExecuteNonQuery();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            LoadCSV();
        }
    }
}
